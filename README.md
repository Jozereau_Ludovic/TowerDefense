<h1>Projet de cours UQAC - Tower Defense</h1>

<p><h2>Description du travail</h2>
Jeu de type Tower Defense. Jeu de stratégie où l'on place des tours pour empêcher des 
ennemis d'atteindre la fin d'un chemin.
</p>

<p><h2>Technologies et langages utilisés</h2>
<ul><li>CLion
    <li>OpenGL
    <li>C++
    <li>Git
</ul></p>

<p><h2>Méthode de travail</h2>
<ul>Méthodologie Agile.
</p>

<p><h2>Rôles dans le projet</h2>
<ul><li>Game Designer
    <li>Programmeur
</ul></p>

<p><h2>Participants</h2>
<ul><li>Stiven Aigle
    <li>Ludovic Jozereau
    <li>Matthieu Lemoing
    <li>Alexis Lethuillier
    <li>Victor Mottet
    <li>Leonor Perquy
</ul></p>

<p><h3>Pas d'exécutable disponible actuellement.</h3></p>