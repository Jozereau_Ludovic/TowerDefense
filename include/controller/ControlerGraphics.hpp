//
// Created by leo on 02/03/16.
//

#ifndef TOWERDEFENSE_CONTROLERGRAPHICS_HPP
#define TOWERDEFENSE_CONTROLERGRAPHICS_HPP


#include "view/Graphics/Graphics.hpp"
#include "engine/tower/Tower.hpp"
#include "engine/enemy/Enemy.hpp"

#include <vector>
#include <memory>
#include "view/Graphics/GraphicsEffect.hpp"

class Regis;
class ControlerGraphics{
private:
    Graphics graphics;
    GraphicsEffect effect;
    Tower *selectedTower = nullptr;
    Regis &regis;

public:
    ControlerGraphics(float widthWindow, float heightWindow, Path *path, Regis &regis);


    template<typename T>
    void display(const std::vector<std::unique_ptr<T> > &displayObject);

    void displayTower(const std::vector<std::unique_ptr<Tower> > &theTower);
    void displayRange(const std::vector<std::unique_ptr<Tower> > &theTower);
    void displayOneRange(Tower *tower);
    // version ok  :
    // void displayEnemy(const std::vector<std::unique_ptr <Enemy> > &theEnemy, bool changeSprite);
    void displayEnemy(const std::vector<std::unique_ptr<Enemy> > &theEnemy);
    void displayProjectile(const std::vector<std::unique_ptr<Projectile> > &theProjectile, bool under = false);
    void displayPath();
    void spriteEnemy(const std::vector<std::unique_ptr <Enemy> > &theEnemy);
    void displayBackground(int useTexture = 0);

    void click(const std::vector<std::unique_ptr<Tower> > &theTower, int x, int y);
    void displayRangeOnClick(const std::vector<std::unique_ptr<Tower> > &theTower);
    void checkSelection(const std::vector<std::unique_ptr<Tower> > &theTower, float x , float y);


    bool isATowerSelected(const std::vector<std::unique_ptr<Tower> > &theTower, int x, int y);


    void setSelectedTower(Tower &tower);
    Tower* getSelectedTower();

    void changeScale(const float x, const float y);
    void displayFreeze(float timeExec);
    void displayUpTower(const  std::vector<std::unique_ptr<Tower>>& theTower);
    void razFreeze();
    Graphics * getGraphics();
    void discretPath();
    void displayOndeDeChoc(vecptr<Projectile> &proj);
    template   <typename T>
    void displayLoad(T load, glm::vec2 scale) { graphics.displayLife(load, scale);}


};


#include "controller/ControlerGraphics.tpp"

#endif //TOWERDEFENSE_CONTROLERGRAPHICS_HPP
