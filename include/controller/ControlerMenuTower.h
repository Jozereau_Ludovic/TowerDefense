//
// Created by lysilia on 04/04/16.
//

#ifndef TOWERDEFENSE_CONTROLERMENUTOWER_H
#define TOWERDEFENSE_CONTROLERMENUTOWER_H




#include "view/Graphics/GraphicsMenuTower.h"
#include <vector>
#include <SDL2/SDL.h>
#include "engine/tower/Tower.hpp"


class Regis;
class ControlerMenuTower {
    private:

        GraphicsMenuTower graphicsMenuTower;
        std::vector<Button> buttonList;
        std::vector<std::function<void()>> actionButtonList;
        glm::vec3 colorAccel{0,0,0};
        const int nbButtons = 2;


    public:
        ControlerMenuTower(int widthWindow, int heightWindow, int numberButton, Regis &regis);
        void display();
        void checkButton(Button &button, float x , float y);
        void click(Tower &tower, int x, int y);
        void creatButtons(int xTower, int yTower);
        void deletButtons();

        bool isAButtonSelected(int x, int y);


    };



#endif //TOWERDEFENSE_CONTROLERMENUTOWER_H
