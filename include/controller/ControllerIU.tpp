
template <typename T>
void ControllerIU::addGhost(glm::vec2 pos, utils::element type) {
    if(!select) {
        select = true;
        button.push_back(
                std::make_unique<TowerButton<T>>(pos[0], pos[1], 50, 50, type, [this, type]{callRegisCreateTower(type);}, false));
    }

}