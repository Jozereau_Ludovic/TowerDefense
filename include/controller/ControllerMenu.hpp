//
// Created by stiven on 21/04/16.
//

#ifndef TOWERDEFENSE_CONTROLLERMENU_HPP
#define TOWERDEFENSE_CONTROLLERMENU_HPP

#include <view/Graphics/Graphics.hpp>
#include <view/userInterface/IUGraphic.hpp>
#include "ControllerIU.hpp"

class ControllerMenu: public ControllerIU{
private:

    Graphics graphics;

public:
    ControllerMenu();
    ControllerMenu(int width, int height, SDL_Window *window, bool *run);
    void display();

};

#endif //TOWERDEFENSE_CONTROLLERMENU_HPP
