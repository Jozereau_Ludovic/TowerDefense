//
// Created by hole on 28/03/16.
//

#ifndef TOWERDEFENSE_CONTROLLERSPELL_H
#define TOWERDEFENSE_CONTROLLERSPELL_H


#include <engine/spell/Spell.h>
#include "engine/enemy/EnemyWave.hpp"
#include "engine/tower/Tower.hpp"
#include <vector>
#include <memory>
#include <string>
#include <map>


class ControllerSpell {
private:
   // std::vector<std::unique_ptr<Spell>> spell;
    EnemyWave wave;
    const std::vector<std::unique_ptr<Enemy>> &enemies;
    const std::vector<std::unique_ptr<Tower>> &towers;
    std::vector<std::unique_ptr<Projectile>> &projectiles;
    std::map <std::string,std::unique_ptr<Spell>> spell;

public:
    ControllerSpell(
        const std::vector<std::unique_ptr<Enemy>> &enemies,
        const std::vector<std::unique_ptr<Tower>> &towers,
        std::vector<std::unique_ptr<Projectile>> &projectiles);

    bool applySpell(std::string key, float x, float y);
    void addSpell(std::string key, std::unique_ptr<Spell> spell);
    int getMana(std::string key);

};

#endif //TOWERDEFENSE_CONTROLLERSPELL_H
