//
// Created by stiven on 12/03/16.
//

#ifndef TOWERDEFENSE_REGIS_HPP
#define TOWERDEFENSE_REGIS_HPP

#include "GameState.hpp"
#include "ControllerIU.hpp"
#include "ControlerGraphics.hpp"
#include "ControlerMenuTower.h"
// cette classe regira tous les autres contrôleurs, c'est lui le papa
/*
 * frameApply : nombre de frame avant chaque affichage
 * loop : nombre de frame ou l'on effectue, le parametre dans le constructeur est en seconde
 * spell : fonction a effectuer
 * elle prend en parametre un booleen qui indique false lorsque l'animation est finie*/
struct DisplaySpell{
    std::function<void(bool raz)> spell{[](bool truc){}};
    int frame = 0;
    int frameApply = 1;
    int loops = 1;
    int applyed = 0;
    DisplaySpell(){};
    DisplaySpell(int frameApply, float loops, std::function<void(bool raz)> spell):spell(spell), frameApply(frameApply), loops((int)(loops*60)){}
    void apply(){
        frame++;
        if(frame == loops){
            spell(true);
            frame = 0;
            applyed = 0 ;
        }else if(!(frame%frameApply)){
            spell(false);
            applyed++;
        }
    }
};

class Regis {
private :
    std::unique_ptr <Tower> towerBeingCreated = nullptr;
    Path path ;
    Path airPath ;
    GameState game;
    ControlerGraphics controlerGraph;
    ControllerIU iu;

    ControlerMenuTower controlerMenuTower;

    int frame ;
    glm::vec2 coorMouse;
    glm::vec2 sizeWindow;
    bool run = true;
    std::map <std::string,bool> spellActive;
    std::map <std::string,DisplaySpell> spellEffect;
    bool isTowerOnPath;
    bool stope = true;
    bool next = false;
    utils::state stateOfGame = utils::state::CONTINUE;

    void displayAoe();
    void prioriteClick(int x, int y);

public:
    Regis();
    Regis(int widthWindow, int heightWindow, SDL_Window *window);
    void displayAll();
    bool play(int x, int y);
    void clickMouse(int x, int y, Uint8 typeButton);
    void incrementFrame();
    void createTower(utils::element type);
    //std::unique_ptr <Tower> createTower(int type);
    void changeAcceleration();
    void pause();

    int useSpell(std::string key, int x = 0, int y = 0);

    void addSpell(std::string key,std::unique_ptr<Spell> newSpell, int cooldown);
    void earthQuake(bool raz);
    void freeze(bool raz);
    void upTower(bool raz);
    void suppTower();
    void upgradeTower();

    void creatButtonsMenuTower(int x, int y);
    void suppButtonsMenuTower();


    void cheat();
    void soundDeath();

    void stop(bool next = false);
    void setTowerBeingCreated(std::unique_ptr <Tower> tower);
    void launch();
    bool getNext();

};

#endif //TOWERDEFENSE_REGIS_HPP
