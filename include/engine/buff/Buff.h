//
// Created by hole on 05/03/16.
//

#ifndef TOWERDEFENSE_BUFF_H
#define TOWERDEFENSE_BUFF_H

#include "utils.hpp"


class Enemy;

class Buff{
protected:

    int efficiency;
    int duration;              /* nb de frames avant fin du buff */
    utils::element elem = utils::element::FIRE;
    int frequency;
    int nextOccur;//TODO DANGER TEST
    bool wasDone=false;
    glm::vec3 buffTower; /* range, frequency, damage */

public:

    Buff();
    Buff(int efficiency,int duration);
    Buff(int efficiency, int duration, utils::element elem);
    Buff(int duration, utils::element elem, glm::vec3 buffTower);


    bool isOver();
    void decrementDuration();
    utils::element getElem();
    virtual void applyBuff(Enemy &enemy) =0;
    virtual int isPoison() = 0;

    int getDuration() const;

    int getEfficiency();

    glm::vec3 getBuffTower();
    int getFrequency(){return frequency;}; //TODO c'est juste pour le test, faudra les supp
    int getNext(){return nextOccur;};
    Buff(int efficiency, int duration, utils::element elem, int next, int freq);
};

#endif //TOWERDEFENSE_BUFF_H
