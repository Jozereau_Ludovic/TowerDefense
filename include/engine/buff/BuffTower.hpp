//
// Created by jozereau on 25/04/16.
//

#ifndef TOWERDEFENSE_BUFFTOWER_HPP
#define TOWERDEFENSE_BUFFTOWER_HPP

#include "engine/buff/Buff.h"

class BuffTower : public Buff {
public:
    BuffTower();
    BuffTower(int duration, utils::element elem, glm::vec3 buffTower);
    void applyBuff(Enemy &enemy) override{}
    int isPoison() override{return 2;}
    void operator=(const BuffTower & orig);
};

#endif //TOWERDEFENSE_BUFFTOWER_HPP
