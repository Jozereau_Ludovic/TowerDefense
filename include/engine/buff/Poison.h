//
// Created by hole on 06/03/16.
//

#ifndef TOWERDEFENSE_POISON_H
#define TOWERDEFENSE_POISON_H


#include "engine/buff/Buff.h"
#include "engine/enemy/Enemy.hpp"
#include "utils.hpp"
class Poison : public Buff{

private:
    //int frequency;             /* fréquence de dégâts */
    //int nextOccur;             /* nb de frames avant prochaine occurrence */

public:
    void applyBuff(Enemy &enemy) override ;
    int isPoison() override{return 1;}
    Poison();
    Poison(int duration, int quantity, int frequency);
    void operator=(Poison &poison);

};



#endif //TOWERDEFENSE_POISON_H
