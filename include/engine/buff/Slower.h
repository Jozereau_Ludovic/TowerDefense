//
// Created by hole on 06/03/16.
//

#ifndef TOWERDEFENSE_SLOWER_H
#define TOWERDEFENSE_SLOWER_H


#include "Buff.h"
#include "engine/enemy/Enemy.hpp"

class Slower : public Buff{
private:
//bool wasDone=false;

public:
    void applyBuff(Enemy &enemy) override ;
    int isPoison() override{return 0;}

    Slower();
    Slower(int efficiency, int duration);

    void operator=(Slower &slow);
};


#endif //TOWERDEFENSE_SLOWER_H
