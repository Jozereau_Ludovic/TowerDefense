//
// Created by diggyhole on 15/02/16.
//

#ifndef TOWERDEFENSE_ENEMY_HPP
#define TOWERDEFENSE_ENEMY_HPP



#include <glm/vec2.hpp>
#include <vector>
#include "engine/buff/Buff.h"
#include <memory>
#include <engine/buff/Poison.h>
#include "utils.hpp"

class Poison;
class Path;
class Projectile;


class Enemy {

private:
    int hp;
    int orientation = 0;
    int hpMax;
    float speed = 0;
    float initSpeed;
    glm::vec2 position;
    glm::vec2 truePosition;
    float progression;
    float size;
    int type = 0;
    bool fly = false;
    float marge;
    std::vector<std::shared_ptr<Buff>> activePoison;
    std::vector<std::shared_ptr<Buff>> activeSlower;
    std::vector<std::shared_ptr<Buff>> buffList;
    utils::element elem = utils::element::WATER;
    static int compteur;
    int id;
    glm::vec2 posInTexture;
    float baseUvy;
    float baseUvx;

    const void calculEnemyUv();
public:
    Enemy();
    Enemy(int hp, float speed, glm::vec2 position, float size, int type, utils::element elem, bool volant = false, float marge = 0.0f);

    bool isFlying();
    bool isDead();
    bool isArrived(Path &path);
    void decremetAllBuff();
    void activeBuff();
    void changeActive();
    void buffUpdate();
    void takeDamage(utils::element type,float damage);
    void takeBuff(std::vector <std::shared_ptr <Buff>> buffs);
    bool collision(Projectile &projo);
    void decrementSpeed(float quantity, utils::element type);
    bool IsHit(Projectile &projo);
    bool isBetter(Buff *buff1, Buff *buff2);

    glm::vec2 targetPos(Path &path);

    void deplacementTheorique(Path &path, float gameSpeed);
    void calculTruePos(std::vector<std::unique_ptr<Enemy>> &vague, Path &path, int k);
    void move(Path &path,  float gamSpeed, std::vector<std::unique_ptr<Enemy>> &vague, int i);



    const float getProgression() const ;
    float getSize();
    glm::vec2 getPosition();
    const glm::vec2 getThPosition();
    int getHpMax();
    int getHp();
    float resistance(utils::element element);

    const bool isInfTo(const Enemy &enemy) const{
        return (progression  < enemy.progression);
    }


    int getOrientation() const {
        return orientation;
    }
    void setOrientation(int orientation) {
        Enemy::orientation = orientation;
    }
    void setPosition(const glm::vec2 &position) {
        Enemy::position = position;
    }
    void setProgression(float progression) {
        Enemy::progression = progression;
    }
    void setHP(int HP){
        Enemy::hp = HP;
    }
    void setSpeed(float Speed){
        Enemy::speed = Speed;
    }
    void setSize(float Size){
        Enemy::size = Size;
    }
    float getSpeed() const {
        return speed;
    }
    float getInitSpeed() const {
        return initSpeed;
    }
    void setElem(utils::element element);


    utils::element getElem() const {
        return elem;
    }
    std::vector<std::shared_ptr<Buff>> &getActivePoison() {
        return activePoison;
    }
    std::vector<std::shared_ptr<Buff>> &getActiveSlower() {
        return activeSlower;
    }

    const glm::vec2 getPosInTexture(){return posInTexture;};
    void setPosInTexture(glm::vec2 pos){posInTexture = pos;}
    const int getType()const{
        return type;
    }
    const float  getBaseY()const{
        return baseUvy;
    }
    const float getBaseX()const {
        return baseUvx;
    }
};


bool operator< (const Enemy &enemy1, const Enemy &enemy2);


#endif //TOWERDEFENSE_ENEMY_HPP
