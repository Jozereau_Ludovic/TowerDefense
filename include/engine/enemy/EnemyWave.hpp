//
// Created by hole on 01/05/16.
//

#ifndef TOWERDEFENSE_ENEMYWAVE_HPP
#define TOWERDEFENSE_ENEMYWAVE_HPP

#include "Enemy.hpp"

class EnemyWave:public Enemy{
private:
    std::vector<std::unique_ptr<Enemy>> enemyWave;
//    int numWave;
  //  int numLevel;
    std::vector<int> delays;
    std::vector<int> endwaves;
    int currentDelay = 0;
    int waitingForWave = 0;
    int isEndWave = 0;

public:
    EnemyWave();
    EnemyWave(int numWave,int numLevel);
    void waveCreation();
    std::vector<std::unique_ptr<Enemy>> waveApplication();
    int remainingEnemies();
    void sendWave();
    int getIsEndWave() const {
		return isEndWave;
	}
	int getCurentDelay() const {
		return currentDelay;
	}
	const int getWaitingForWave(){
		return waitingForWave;
	}
};

#endif //TOWERDEFENSE_ENEMYWAVE_HPP
