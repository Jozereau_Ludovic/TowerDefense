//
// Created by jozereau on 21/03/16.
//
#pragma once

#include "engine/map/Neutral.hpp"
#include "engine/tower/Tower.hpp"

class Forest : public Neutral {

public:
    Forest(glm::vec2 pos, utils::element type, Object * object, float modifier);

    void apply(Tower * tower);

};

