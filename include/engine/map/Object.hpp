//
// Created by jozereau on 21/03/16.
//
#pragma once


class Object {

protected:
    const bool breakable;
    const float price;
    const float modifier;
    const int type;

public:
    Object(bool breakable, float price, float modifier, int type);

    const bool getBreakable();

    const float getPrice();

    const float getModifier();

    const int getType();
};

