//
// Created by hole on 07/03/16.
//

#ifndef TOWERDEFENSE_PATH_H
#define TOWERDEFENSE_PATH_H
#include <memory>
#include <vector>
#include "SegmentMere.h"
#include <string>
#include "engine/enemy/Enemy.hpp"


class Path {
private:
    std::vector<std::unique_ptr<SegmentMere> > way;
    float totalLength;
    std::vector<glm::vec2> pointsOfPath;
    std::vector<glm::vec2> normaleTab;
    float pas = 1.0f;



public:
    glm::vec2 getPosition(float t);
    glm::vec2 getNormale(float t);
    glm::vec2 getPositionWithFct(float t);


    int discretPath(float i);

    Path();

    Path(std::string fileName);

    float getTotalLength() const {
        return totalLength;
    }

    const std::vector<glm::vec2>& getPath(){return pointsOfPath;}
    const float getPas(){return pas;}
    void addFinalPoint();
};


#endif //TOWERDEFENSE_PATH_H
