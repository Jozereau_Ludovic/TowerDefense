//
// Created by hole on 11/03/16.
//

#ifndef TOWERDEFENSE_BEZIER_H
#define TOWERDEFENSE_BEZIER_H

#include <glm/glm.hpp>
#include "SegmentMere.h"

class SegBezier : public SegmentMere{
private:
    glm::vec2 b1;
    glm::vec2 b2;
    glm::vec2 b3;
    glm::vec2 b4;


public:
    SegBezier();
    SegBezier(glm::vec2 b1, glm::vec2 b2, glm::vec2 b3, glm::vec2 b4);
    float longueur(float bSup = 1.f);
    glm::vec2 calculPos(float t) override ;
    glm::vec2 calculPosReele(float t) override ;

};


#endif //TOWERDEFENSE_BEZIER_H
