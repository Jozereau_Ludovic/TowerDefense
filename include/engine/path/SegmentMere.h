//
// Created by hole on 12/03/16.
//

#ifndef TOWERDEFENSE_SEGMENTMERE_H
#define TOWERDEFENSE_SEGMENTMERE_H
#include <glm/glm.hpp>

class SegmentMere {
protected:
    float length;

public:
    virtual glm::vec2 calculPos(float t)=0;
    virtual glm::vec2 calculPosReele(float t)=0;
    float getLength();
    SegmentMere();

};


#endif //TOWERDEFENSE_SEGMENTMERE_H
