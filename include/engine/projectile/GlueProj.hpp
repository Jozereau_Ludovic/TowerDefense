//
// Created by stiven on 11/05/16.
//

#ifndef TOWERDEFENSE_GLUEPROJ_HPP
#define TOWERDEFENSE_GLUEPROJ_HPP

#include "Projectile.h"
struct GlueProj : public Projectile{

    GlueProj(glm::vec2 pos, glm::vec2 vit, float damage, std::vector<std::shared_ptr<Buff>> &buff,
                 int numberEnemy, utils::element element, int countdown);
    void move(float accelGame) override ;

};
#endif //TOWERDEFENSE_GLUEPROJ_HPP
