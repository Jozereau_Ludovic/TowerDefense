//
// Created by stiven on 07/04/16.
//

#ifndef TOWERDEFENSE_MIROIR_HPP
#define TOWERDEFENSE_MIROIR_HPP

#include <engine/path/Path.h>
#include "Projectile.h"


class Miroir : public Projectile{
private:
    Path &path;
    float progression;
    float spd = -4;
    int pas = 10;
    int change = 0;
    int timeToChange = 4;

    void changeRotate();
public:

    Miroir(Path& path);
    Miroir(Path& path,float damage);

    void move(float accel);
    void shorten();

};




#endif //TOWERDEFENSE_MIROIR_HPP
