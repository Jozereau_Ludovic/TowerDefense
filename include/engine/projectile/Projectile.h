#pragma once


#include <glm/vec2.hpp>
#include <vector>
#include "engine/buff/Buff.h"
#include <memory>
#include "utils.hpp"

class Enemy;



class Projectile{



protected:
    glm::vec2 speed;
    glm::vec2 position;
    float damage;
    glm::vec2 size ;
    utils::element type = utils::element::FIRE;
    std::vector <std::shared_ptr <Buff>> vbuff;
    int numberEnemy = 10;
    int countdown;
    bool isDetectedByEnemy;
    float rotation = 0.0f;
    std::vector <int> touch;

public:
    Projectile();
    Projectile(glm::vec2 pos, glm::vec2 vit, float damage, std::vector <std::shared_ptr <Buff>> &buff,int numberEnemy, utils::element element, int countdown = -1, bool touche = true);
    Projectile(glm::vec2 pos, glm::vec2 vit, float damage, std::vector <std::unique_ptr <Buff>> &buff, utils::element element, int countdown = -1, bool touche = true);
    Projectile(glm::vec2 pos, glm::vec2 vit, float damage, std::vector <std::unique_ptr <Buff>> &buff,int numberEnemy, utils::element element, int countdown = -1, bool touche = true);

    Projectile(int numberEnemy);
    Projectile(float damage, int numberEnney, int countdown);

    const glm::vec2 getSpeed();
    void setSpeed(glm::vec2 vit);
    void setSize(glm::vec2 size){ this->size = size; }

    const glm::vec2 getPosition();

    bool getIsDetectedByEnemy();

    virtual void move(float accel);
    float getRotate(){return rotation;}
    const float getDamage();

    glm::vec2 getSize();

    utils::element getElement() const ;

    const std::vector <std::shared_ptr <Buff>> &getVbuff() const ;

    void aoe (std::vector <std::unique_ptr<Enemy>> &enemy, int range = 100);

    void decrementEnemy();

    bool usable();

    void decrementation();

    bool isnull();
    const utils::element getType();

    const std::vector<int> &ajoutId (int id);

    bool isAlreadyTouch(int id);


//    void update();

};