//
// Created by hole on 29/03/16.
//

#ifndef TOWERDEFENSE_CIBLEEFFECT_H
#define TOWERDEFENSE_CIBLEEFFECT_H

#include "Spell.h"

class CibleEffect : public Spell{
private:
public:
    bool apply(float x, float y, const std::vector<std::unique_ptr<Tower>> &towers,
               const std::vector<std::unique_ptr<Enemy>> &enemies,
               std::vector<std::unique_ptr<Projectile>> &projectiles)  override;

};


#endif //TOWERDEFENSE_CIBLEEFFECT_H
