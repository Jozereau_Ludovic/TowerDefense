//
// Created by hole on 29/03/16.
//

#ifndef TOWERDEFENSE_CIBLEPROJO_H
#define TOWERDEFENSE_CIBLEPROJO_H

#include "Spell.h"

class Glue : public Spell{
private:
    int nbProjo;
    int nbEnemy;
    Path &path;
public:
    bool apply(float x, float y, const std::vector<std::unique_ptr<Tower>> &towers,
               const std::vector<std::unique_ptr<Enemy>> &enemies,
               std::vector<std::unique_ptr<Projectile>> &projectiles) override;

    Glue(int price,
           int coolDown,
         utils::element element,
           float damage,
           std::vector<std::shared_ptr<Buff>> buffList,int nbProjo,Path &path, int nbEnemy);


};

#endif //TOWERDEFENSE_CIBLEPROJO_H

