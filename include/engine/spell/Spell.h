//
// Created by hole on 25/03/16.
//

#ifndef TOWERDEFENSE_SPELL_H
#define TOWERDEFENSE_SPELL_H




//TODO : si il y a une enum général pour les éléments, le mettre là



#include <vector>
#include "engine/buff/Buff.h"
#include "engine/tower/Tower.hpp"
#include "engine/projectile/Projectile.h"

class Spell {
protected:
    int price;
    int coolDown;
    bool needTarget;
    utils::element element;
    float damage;
    std::vector<std::shared_ptr<Buff>> buffList;

public:

    Spell(int price,
          int coolDown,
          utils::element element,
          float damage):price(price), coolDown(coolDown), element(element), damage(damage){};


    Spell(int price,
          int coolDown,
          utils::element element,
          float damage,
          std::vector<std::shared_ptr<Buff>> buffList):price(price), coolDown(coolDown), element(element), damage(damage), buffList(buffList){};

    virtual bool apply(float x, float y, const std::vector<std::unique_ptr<Tower>> &towers,
                       const std::vector<std::unique_ptr<Enemy>> &enemies,
                       std::vector<std::unique_ptr<Projectile>> &projectiles) = 0;
    int getMana() const{return price;}
    virtual float getRange(){return 0;}

};




#endif //TOWERDEFENSE_SPELL_H
