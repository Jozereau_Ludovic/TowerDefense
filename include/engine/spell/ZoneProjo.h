//
// Created by hole on 29/03/16.
//

#ifndef TOWERDEFENSE_ZONEPROJO_H
#define TOWERDEFENSE_ZONEPROJO_H

#include "Spell.h"

class ZoneProjo : public Spell{
private:
    float range;
public:
    bool apply(float x, float y, const std::vector<std::unique_ptr<Tower>> &towers,
               const std::vector<std::unique_ptr<Enemy>> &enemies,
               std::vector<std::unique_ptr<Projectile>> &projectiles)  override;
    float getRange() override;
};

#endif //TOWERDEFENSE_ZONEPROJO_H
