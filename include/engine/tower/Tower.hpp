//
// Created by jozereau on 15/02/16.
//

#pragma once

#include <glm/vec2.hpp>
#include <engine/buff/BuffTower.hpp>
#include "engine/enemy/Enemy.hpp"
#include "engine/path/Path.h"
#include "utils.hpp"


class Tower {

protected:
    const glm::vec2 pos;
    const float vit = 5.0f;
    utils::element type;
    int level;
    float range;
    float initRange;
    float damage;
    float initDamage;
    float frequency;
    float initFrequency;
    float price;
    Enemy *target = nullptr;
    std::vector <std::unique_ptr <Buff>> buff; /* projectiles */
    std::vector <std::unique_ptr <Buff>> buffTower; /* sur la tour */

    bool mouseSelection = false;    // flag : vrai si le curseur est sur la tour
    bool selected = false;          // flag : vrai si on a cliqué (selectionner) sur la tour


public:
    Tower();
    Tower(glm::vec2 pos, utils::element type, float range, float degats, float frequency, float price);
    Tower(glm::vec2 pos, utils::element typ);        // pour la towerButton

    const glm::vec2 getPosition();

    const utils::element getType();

    const int getLevel();
    void upgrade();

    const float getRange();
    void setRange(float range);

    const float getDamage();
    void setDamage(float damage);

    const float getFrequency();
    void setFrequency(float frequency);

    const float getPrice();
    void setPrice();

    const Enemy * getTarget();

    bool detect(Enemy * enemy);

    virtual std::unique_ptr <Projectile> shoot(Path &path, float gameSpeed);
    void reset();

    void insertBuffTower(std::vector <std::shared_ptr <Buff>> buff);
    void activateBuffTower();
    void desactivateBuffTower();
    void updateBuffTower();

    // Vérifie si le curseur est sur la tour
    bool isMouseSelection(int xMouse, int yMouse);// return true, if the cursor is on the tower
    bool getMouseSelection();
    void setMouseSelection(bool select);

    bool getSelected();
    void setSelected(bool select);
    bool isUp();

    void operator=(Tower &tower);

};

