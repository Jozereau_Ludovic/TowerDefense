//
// Created by leo on 02/03/16.
//

#ifndef TOWERDEFENSE_UTILS_HPP
#define TOWERDEFENSE_UTILS_HPP


#include <stdlib.h>
#include <iostream>
#include <view/sound/Sound.hpp>
#include <vector>
#include <glm/glm.hpp>


/*les quatres premiers bits represente les lignes
 * les quatres dernier les colonnes*/
template <typename T>
using vecptr = std::vector<std::unique_ptr<T>> ;

namespace utils {
    static Sound sound;
    constexpr int widthWindow = 1200;
    constexpr int heightWindow = 700;


    enum class element: int{FIRE=0b00010001,EARTH=0b00010010,WATER=0b00011000,AIR=0b00010100,NEUTRAL=0b00000000,
                        GLUE = 0b00100001, METEORE = 0b00100010, MIRROR = 0b00101000};

    enum class state: int {CONTINUE, LOSE, WIN};


//calcul la zone de texture a utiliser
    template<typename T>
    glm::vec2 calculUv( T &toto) {
        constexpr float decaleUv = 0.25f;
        constexpr float decaleUVy  = 0.5f;
        bool colonne = true;
        int type = (int)toto.getType();

        float x = 0.0f, y = 0.0f;
        int bit;

        for (int i = 0; i < 8 ; i++) {

            bit = (type>>i) & 1 ;

                if (colonne && bit && i < 4) {
                    x = i * decaleUv;
                    colonne = false;
                } else if(bit) {
                    y = (i-4) * decaleUVy;


                }


        }
        return glm::vec2(x, y);
    }
    template <typename T>
    glm::mat3 normalized(const T width, const T height, const float deplaceX,const float deplaceY){
        return glm::mat3{
                2.0f / width, 0.0f, 0.0f,
                0.0f, -2.0f / height, 0.0f,
                -deplaceX, deplaceY, 1.0f
        };
    }

}

#endif //TOWERDEFENSE_UTILS_HPP
