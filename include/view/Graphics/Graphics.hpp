//
// Created by leo on 22/02/16.
//

#ifndef TOWERDEFENSE_GRAPHICS_H
#define TOWERDEFENSE_GRAPHICS_H


#include <GLifier/macro.hpp>
#include <GLifier/Program.hpp>
#include <GLifier/Vao.hpp>
#include <GLifier/Vbo.hpp>
#include <GLifier/quad.hpp>
#include <GLifier/Texture.hpp>
#include <GLifier/displayText.hpp>
#include "view/userInterface/Button.hpp"
#include "engine/projectile/Projectile.h"
#include <vector>
#include <string>

#include <glm/gtc/type_ptr.hpp>
#include "engine/path/Path.h"

extern const float numberWidthPicturesTour;
extern const float numberHeightPicturesTour;

extern const float widthPictureTower;
extern const float heightPictureTower;

extern const float numberWidthPicturesEnemy;
extern const float numberHeightPicturesEnemy;

extern float widthPictureEnemy;
extern float heightPictureEnemy;

extern float widthPictureProjectile;
extern float heightPictureProjectile;



class Tower;
class Enemy;
class Graphics {
protected:
    libgl::Vao vao;
    libgl::Vbo vboRect;
    libgl::Vbo vboCoord;
    // uniform = transmet les infos des .cpp aux shader
    libgl::Vbo vboPath;
    libgl::Vao vaoPath;
    libgl::Vbo vboRectPath;
    libgl::Vbo vboCoordText;
    libgl::Texture textureTower;
    libgl::Texture textureEnemy;
    libgl::Texture textureProjectile;
    libgl::Texture textureBackground{"../resources/images/background.png"};
    libgl::Texture texturePath{"../resources/images/path2.png"};
    libgl::Texture textureFreeze{"../resources/images/freeze.png"};
    float posHtextureEnemy; // x du sprite de l'enemy, mis a jour a chaque frame
    float posVtextureEnemy; // y du sprite de l'enemy, idem
    int widthWindow;
    int heightWindow;



    libgl::transformStorage transformStruct;   // structure qui contient l'angle de rotation, les proportions de transformations, ...
    LIBGL_CREATE_PROGRAM(Range, "../sources/view/shader/VertexGraphics.glsl", "../sources/view/shader/FragmentRange.glsl", (scale)(tran)(uvHG)(pictureSize)(texture2D)(spell)(onPath))
    LIBGL_CREATE_PROGRAM(Barre, "../sources/view/shader/healthVertex.glsl", "../sources/view/shader/healthFragment.glsl", (scale)(tran)(hp)(hpMax))
    LIBGL_CREATE_PROGRAM(display, "../sources/view/shader/VertexGraphics.glsl","../sources/view/shader/FragmentGraphics.glsl",(scale)(tran)(uvHG)(texture2D)(pictureSize)(change)(alphas)(useTexture))

    LIBGL_CREATE_PROGRAM(pathEnemy,"../sources/view/shader/pathVertex.glsl", "../sources/view/shader/pathFragment.glsl",
                         (scale)(tran)(texture2D))
    Path *path = nullptr;
    std::vector<glm::vec2> pointPath;
    int numberPoint;
    void triangulize();



public:

    Graphics(std::string pathTextureEnemy, std::string pathTextureProjectile, int widthWindow,
                 int heightWindow, std::string pathTextureTower, Path *path);

    Graphics(int width, int height);
    Graphics();
    void discretPath();
    template<typename T1>
    void displayT(T1 &position);

    void displayTower(Tower &tower);
    // version ok :
    //void displayEnemy(Enemy &enemy, bool changeSprite);
    void displayEnemy(Enemy &enemy);

    void displayProjectile(Projectile &projectile, int useTexture = 0);
    void displayPath();

   // void displayRange(Tower &tower);

    void spriteEnemy(int orientation, Enemy &enemy);
    void spriteEnemyRight(const Enemy &enemy);
    void spriteEnemyLeft(const Enemy &enemy);
    void spriteEnemyAhead(const Enemy &enemy);
    void spriteEnemyBehind(const Enemy &enemy);

    void displayBackground(int useTexture = 0);
    void changeScale(const float x, const float y);
    template <typename T>
    void displayRange(T *tower, int spell = 0, float range = 0, float center = 0.5, int onPath = 0);
    template <typename T>
    void displayLife(T &enemy, glm::vec2 scale = glm::vec2{50,10}) ;


};

#include "view/Graphics/Graphics.tpp"


#endif //TOWERDEFENSE_GRAPHICS_H
