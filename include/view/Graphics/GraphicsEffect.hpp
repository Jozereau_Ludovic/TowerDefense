//
// Created by stiven on 23/04/16.
//

#ifndef TOWERDEFENSE_GRAPHICSEFFECT_HPP
#define TOWERDEFENSE_GRAPHICSEFFECT_HPP

#include "Graphics.hpp"
class Tower;
class GraphicsEffect : public Graphics{

private:
    float alpha = 1 ;
    libgl::Texture textureFreeze{"../resources/images/freeze.png"};
    libgl::Texture textureStar{"../resources/images/star.png"};

public:

    GraphicsEffect(int width, int height);
    void displayFreeze(float timeExec);
    void razAlpha();
    void displayUpTower(const std::unique_ptr<Tower> &tower, int use);
    void displayOndeDeChoc(std::unique_ptr<Projectile> &proj);


};

#endif //TOWERDEFENSE_GRAPHICSEFFECT_HPP
