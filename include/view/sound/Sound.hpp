//
// Created by unnom on 20/03/16.
//

#ifndef POULPINATOR_SOUND_HPP
#define POULPINATOR_SOUND_HPP

#include <SDL2/SDL_mixer.h>
#include <map>
#include <string>

class Sound {
private :
    std::map<std::string,Mix_Chunk *> sons;
    int chanelUsed = 0;
    static int volume ;
    static bool pause ;
public:
    Sound();
    void play(std::string);
    static void pauseAll();
    static bool getPause();
    ~Sound();

};


#endif
