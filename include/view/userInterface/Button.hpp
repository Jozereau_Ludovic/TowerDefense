//
// Created by stiven on 15/02/16.
//

#ifndef TOWERDEFENSE_BUTTON_HPP
#define TOWERDEFENSE_BUTTON_HPP

#include <glm/glm.hpp>
#include <functional>
#include <string>
#include "utils.hpp"

class Button {
protected:
    glm::vec2 coordinate;
    glm::vec2 size;
    bool selected = false;
    utils::element type;
    std::function<void()> action;
    bool clickable = true;
    bool once = false;

public:
    Button();
    Button(float x, float y, int width, int height, utils::element type, std::function<void()> action);
    Button(float x, float y, int width, int height, utils::element type, std::function<void()> action, bool text);
    Button(float x, float y, int width, int height, utils::element type);
    Button(Button&& orig);
    bool isSelected(int xMouse, int yMouse);// return true, if the cursor is on the Button
    glm::vec2 const &getPosition() const;
    glm::vec2 const &getSize() const;
    bool getSelected();
    void setSelected(bool select);
    void setCoor(float x, float y);
    void setY(float y);
    const utils::element getType() const;
    void setClickable(bool click);
    const int getOnce(){return once;}
    virtual void useButton();
    virtual float getRange(){return 0;};
    void changeFunction(std::function<void()> function );

};


#endif //TOWERDEFENSE_BUTTON_HPP
