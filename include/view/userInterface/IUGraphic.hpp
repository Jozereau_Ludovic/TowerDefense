//
// Created by stiven on 15/02/16.
//

#ifndef TOWERDEFENSE_IUGRAPHIC_HPP
#define TOWERDEFENSE_IUGRAPHIC_HPP


#include <vector>
#include <string>
#include <GLifier/macro.hpp>
#include <GLifier/Program.hpp>
#include <GLifier/Vao.hpp>
#include <GLifier/Vbo.hpp>
#include <GLifier/quad.hpp>
#include <GLifier/Texture.hpp>
#include <GLifier/displayText.hpp>
#include "utils.hpp"


constexpr float decaleUv = 0.25f;
struct StatPlayer;
class Button;
class SpellButton;
class IUGraphic {
private:
    libgl::Vao vao;
    libgl::Vbo vboRect;
    libgl::Vbo vboUv;
    libgl::Vbo vboSpell;
    LIBGL_CREATE_PROGRAM(program,"../sources/view/shader/IUVertex.glsl","../sources/view/shader/IUFragment.glsl",
                         (scale)(transformation)(texture2D)(surbrillance)(alpha)(hg)(bd))
    libgl::Texture textureTower;
    libgl::Texture textureSpell;
    libgl::Texture textureSound{"../resources/images/sound.png"};
    libgl::transformStorage transformStruct;
    LIBGL_DISPLAYTEXT(displayText, "../sources/view/shader/vertexText.glsl", "../sources/view/shader/fragmentText.glsl",
                      "../resources/images/font.png", glm::vec2(0), colorText, couleur, alpha)
    LIBGL_CREATE_PROGRAM(interface, "../sources/view/shader/interfaceVertex.glsl", "../sources/view/shader/interfaceFragment.glsl",(scale)(tran)(alpha)(end)(sound))
    LIBGL_CREATE_PROGRAM(spellDisplay,"../sources/view/shader/spellVertex.glsl","../sources/view/shader/spellFragment.glsl",
                         (scale)(tran)(cooldown)(currentCooldown)(texture2D)(hg)(bd)(wait)(surbrillance))
    int width;
    int height ;
    float alpha = 0.0f;

public:

    IUGraphic();
    IUGraphic(std::string pathTextureTower, std::string pathTextureSpell, int widthWindow, int heightWindow);
    IUGraphic(int widthWindow, int heightWindow);
    void displayTextButton(Button &button, std::string text);
    void displayTower(Button &button, int sound = 0);
    void changeAlpha(float alpha);
    void changeColor (glm::vec3 color);
    void displayRect();
    void displayStat(const StatPlayer &stat);
    void displaySpellButton( SpellButton & spellButton);
    void displayCooldownWave(Button &button, float actualCool, float maxCool);
    glm::vec2 chooseSpellImage(SpellButton &spell);
    void displayEnd(utils::state state, Button &end);



};


#endif //TOWERDEFENSE_IUGRAPHIC_HPP
