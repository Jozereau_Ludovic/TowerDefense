//
// Created by stiven on 07/04/16.
//

#ifndef TOWERDEFENSE_SPELLBUTTON_HPP
#define TOWERDEFENSE_SPELLBUTTON_HPP

#include <string>
#include "view/userInterface/Button.hpp"


class SpellButton : public Button{
public:
    using Action = std::function<bool()>;
private:
    bool active = false;
    float cooldown;
    float currentCooldown;
    Action action;
    std::string key;
    float range;


public:

    SpellButton(int x, int y, int width, int height, utils::element type, Action action, float cooldown,
                std::string key);
    SpellButton(int x, int y, float range, utils::element type, Action action, std::string(key));
    void useButton() override ;
    void enable();
    void decreaseCooldown();
    bool isActive();
    float getCooldown();
    float getCurrent();
    std::string getKey();
    float getRange() override ;



};

#endif //TOWERDEFENSE_SPELLBUTTON_HPP
