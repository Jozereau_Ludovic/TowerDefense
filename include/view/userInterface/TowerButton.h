//
// Created by lysilia on 15/05/16.
//

#ifndef TOWERDEFENSE_TOWERBUTTON_H
#define TOWERDEFENSE_TOWERBUTTON_H


#include "engine/tower/Tower.hpp"
#include "Button.hpp"
#include "engine/tower/Fire.hpp"
template <class T>
class TowerButton : public Button {
    T tower{glm::vec2(0)};
public:
    TowerButton();
    TowerButton(int x, int y, int width, int height, utils::element type, std::function<void()> action, bool text);
    float getRange() override ;



};





#endif //TOWERDEFENSE_TOWERBUTTON_H
