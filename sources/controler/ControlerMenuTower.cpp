//
// Created by lysilia on 03/04/16.
//

#include <iostream>
#include "controller/ControlerMenuTower.h"
#include "controller/Regis.hpp"


ControlerMenuTower::ControlerMenuTower(int widthWindow, int heightWindow, int numberButton, Regis &regis)
        : graphicsMenuTower(std::string("../resources/images/menuTower1.png"),
                            widthWindow,
                            heightWindow)
        {

    /*
     * pour ajouter un bouton : on rajoute une action, on incremente le nbButtons
     * on met a jour le nb d'image aussi pour l'affichage
     */
    // creation de la liste des actions des boutons du menu des tours

    // action premier bouton : suppression de la tour
    actionButtonList.push_back([&regis](){  //fatal error: call to implicitly-deleted copy constructor of 'Regis' actionButtonList.push_back([this, regis](){
        regis.suppTower();}
    );
    // action second bouton : upgrade de la tour
    actionButtonList.push_back([&regis](){
        regis.upgradeTower();}
    );
}


/**
 * Affichage des boutons
 */
void  ControlerMenuTower::display() {
   // graphicsMenuTower.displayRect(); // dans l'iu, c'est le rect orange de fond du menu
    size_t size = buttonList.size();
   // graphicsMenuTower.changeColor(glm::vec3(0.,0.,0.));
    //graphicsMenuTower.displayTextButton(buttonList[0],"Menu");
    //graphicsMenuTower.changeColor(colorAccel);
   // graphicsMenuTower.displayTextButton(buttonList[1],">>");
    for (int i = 0 ; i < size ; i++){
        graphicsMenuTower.displayButtons(buttonList[i], i);
    }
}


/**
 * Met à jour le flag de selection du bouton -> pour savoir s'il on a cliqué dessus
 */
void ControlerMenuTower::checkButton(Button &button, float x, float y) {

    if(button.isSelected(x, y)){
        button.setSelected(true);
    }
    else{
        button.setSelected(false);
    }
}


/**
 * Action du click : action du botuon cliqué
 */
void ControlerMenuTower::click(Tower &tower, int x, int y) {
    for (auto &iter : buttonList){
        checkButton(iter, x, y);
        if(iter.getSelected()){
            iter.useButton();
        }
    }
}


/**
 * Création des boutons
 */
/*
void ControlerMenuTower::creatButtons(int xTower, int yTower){
    int widthButton = 30;
    int  heightButton = 30;
    int posX = xTower + 80 + widthButton;
    int posY = yTower + 10 + heightButton;

    for(int i = 0 ; i < nbButtons ; i++){ // creation des boutons
        buttonList.push_back(Button(posX - heightButton - 50 * (i + 1), posY, widthButton, heightButton,
                                    (int)std::pow(10, i),
                                    actionButtonList[i]));
        // on s'assure que sa selection soit a faux
        buttonList[i].setSelected(false);
    }
}*/

void ControlerMenuTower::creatButtons(int xTower, int yTower){
    int widthButton = 20;          // taille du bouton
    int  heightButton = widthButton;

    float rayon = 50;              // rayon du cercle des boutons

    glm::vec2 vect = glm::vec2(0, rayon);       // vecteur pour positionner les boutons

    float posX;                    // position des boutons
    float posY;

    float theta;                   // angle de rotation

    double rsin;                   // pour le calcul de la rotation
    double rcos;

    // calcul de l'angle
    theta = (float) (2 * (M_PI) / nbButtons);
    rsin = sin(theta);
    rcos = cos(theta);

    for(int i = 0 ; i < nbButtons ; i++){
        // mis a jour des positions des boutons
        posX = vect.x + xTower - (widthButton / 2);
        posY = vect.y + yTower - (heightButton / 2);

        // creation des boutons
        buttonList.push_back(Button(posX, posY,
                                    widthButton, heightButton,
                                    utils::element::NEUTRAL,
                                    actionButtonList[i]));

        // calcul du nouveau vecteur : rotation
        glm::vec2 newVect = glm::vec2(vect.x * rcos - vect.y * rsin, vect.x * rsin + vect.y * rcos);

        vect = newVect;             // mis a jour du vecteur de position


        // on s'assure que sa selection soit a faux
        buttonList[i].setSelected(false);
    }
}


/**
 * Suppression des boutons
 */
void ControlerMenuTower::deletButtons(){
    if(!buttonList.empty())
        while(!buttonList.empty()){
            buttonList.pop_back();
        }
}



/**
 * Vérifie s'il y a un bouton sélectionné
 */
bool ControlerMenuTower::isAButtonSelected(int x, int y){
    bool resu = false;
    for(auto &iter : buttonList){
        if(iter.isSelected(x, y))
            resu = true;
    }
    return resu;
}