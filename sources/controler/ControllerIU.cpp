//
// Created by stiven on 16/02/16.
//


#include <iostream>
#include "engine/tower/Earth.hpp"
#include "engine/tower/Water.hpp"
#include "engine/tower/Wind.hpp"
#include "engine/tower/Fire.hpp"
#include "controller/ControllerIU.hpp"
#include "controller/Regis.hpp"
#include "view/userInterface/TowerButton.h"
#include "../sources/view/userInterface/TowerButton.cpp"
#include "view/userInterface/SpellButton.hpp"
#include "view/userInterface/Button.hpp"
#include "utils.hpp"


ControllerIU::ControllerIU(int widthWindow, int heightWindow, int numberButton, Regis *regis, Graphics *graphics)
        : iuGraphic(std::string("../resources/images/tour.png"),
                    ("../resources/images/spell.png"), widthWindow, heightWindow)
        , graph(graphics), regis(regis), widthWindow(widthWindow), heightWindow(heightWindow),
          end(widthWindow/2-50, heightWindow/2-50, 100, 20, utils::element::NEUTRAL, [regis]{
              regis->stop(true);
          }){
     widthButton = widthWindow/16;
    float posX = widthWindow-1.25f*widthButton;
    float posY = heightWindow/16;
    float  heightTower = (heightWindow-posY)/numberButton;
    findMiddle(end);
    /*
     * Le dernier parametre correspond à la fonction que va contenir le bouton,
     * j'envoie une lambda
     *
     * syntaxe []{}, entre crochet on met les éléments que l'on veut capturer dans la lambda
     * autrement dit ce dont la lambda a besoin, par exemple dans l'exemple du dessous
     * j'ai besoin d'une reference vers regis donc je la capture, et entre crochet on met ce que l'on veut
     * faire, avec la même syntaxe qu'un code cpp habituel */

    // Création des boutons de l'iu
    button.push_back(std::make_unique<Button>(posX, 0, widthButton, posY, utils::element::NEUTRAL, [regis] { regis->pause();
                                                                        }, false));
    //button.emplace_back(posX-widthButton,0,widthButton,posY, 0,std::bind(&Regis::changeAcceleration, std::ref(regis)));
    button.push_back(std::make_unique<Button>(posX-widthButton,0,widthButton/2,posY, utils::element::NEUTRAL,[regis,this]{regis->changeAcceleration();
        setColorAccel();}));
    button.push_back(std::make_unique<Button>(posX-widthButton,posY,widthButton/2,widthButton/2, utils::element::NEUTRAL,[regis]{regis->launch();}));
    button.push_back(std::make_unique<Button>(posX-2*widthButton,posY/2,widthButton/2,posY, utils::element::NEUTRAL,[]{
        utils::sound.pauseAll();
    }));

    //spellButton[0].useButton();
    utils::element towerElement [] ={utils::element::FIRE, utils::element::EARTH, utils::element::WATER, utils::element::AIR};

    for(int i = 0;i<numberButton;i++){ // création des boutons
        button.push_back(std::make_unique<Button>(posX, posY + i * heightTower, widthButton, heightTower, towerElement[i],
                                []{}, false));

    }
    int i = 4;
    button[i++]->changeFunction([this, posX, posY, towerElement]{addGhost<Fire>(glm::vec2(posX, posY), towerElement[0]);});
    button[i++]->changeFunction([this, posX, posY, towerElement]{addGhost<Earth>(glm::vec2(posX, posY), towerElement[1]);});
    button[i++]->changeFunction([this, posX, posY, towerElement]{addGhost<Water>(glm::vec2(posX, posY), towerElement[2]);});
    button[i]->changeFunction([this, posX, posY, towerElement]{addGhost<Wind>(glm::vec2(posX, posY), towerElement[3]);});
    int posXMenu = widthWindow/2 - 50;
    constexpr int heightButton = 30;
    constexpr int widthButtonMenu = 100;
    buttonMenu.emplace_back(posXMenu, 0, widthButtonMenu, heightButton, utils::element::NEUTRAL,[regis]{regis->pause();});
    buttonMenu.emplace_back(posXMenu, 0, 50, heightButton, utils::element::NEUTRAL,[]{ utils::sound.pauseAll();});
    buttonMenu.emplace_back(posXMenu, 0, widthButtonMenu, heightButton, utils::element::NEUTRAL,[regis]{
        regis->stop(false);});
    for(auto &button :buttonMenu){
        findMiddle(button);
    }



}

ControllerIU::ControllerIU(int width, int height):iuGraphic(width,height), widthWindow(width),heightWindow(height) {

}




// affiche les boutons
/*Je pense que la dedans que tu vas devoir travailler vu que c'est la fonction d'affichage
 * l'affichage est effectuer en fonction de la position dans le vector
 * exemple on sait que le premier type du vector et le bouton Menu
 * et on sait que le tout dernier sera soit un bouton de tour immobile
 * soit si on a appuyer sur un bouton de tour ce sera la "tour" que tu deplace avec la souris
 * pour pouvoir la placer donc c'est la que tu devra agir pour afficher la range
 *
 * Oui c'est pas terrible d'afficher les boutons de cette manière mais pour l'instant j'ai pas trouver mieux */
void  ControllerIU::display(const StatPlayer &stat, float cooldown, float cooldownMax, bool onPath) {
    iuGraphic.displayRect();
    size_t size = button.size();
    iuGraphic.changeColor(glm::vec3(0.,0.,0.));
    iuGraphic.displayTextButton(*button[0],"Menu");
    iuGraphic.changeColor(colorAccel);
    iuGraphic.displayTextButton(*button[1],">>");
    if(cooldown != cooldownMax || !button[2]->getOnce()) {
        iuGraphic.displayCooldownWave(*button[2], cooldown, cooldownMax);
        button[2]->setClickable(true);
    }else{
        button[2]->setClickable(false);
    }
    iuGraphic.displayTower(*button[3],1);
    for (int i = 4 ; i < size ; i++){
        iuGraphic.displayTower(*button[i], 0);

    }
    if(select){
        graph->displayRange(button.back().get(), 0, 0, 0.0f, onPath);
    }
    iuGraphic.displayStat(stat);
    for(auto &spell :spellButton){
        iuGraphic.displaySpellButton(spell);
    }
    if(spell){
        graph->displayRange(spell.get());
    }

}

void ControllerIU::displayMenu() {
    iuGraphic.changeColor(glm::vec3(0.0f, 0.0f, 1.0f));
    iuGraphic.displayTextButton(buttonMenu[0],"Reprendre");
    iuGraphic.displayTextButton(buttonMenu[1],"Son");
    iuGraphic.displayTextButton(buttonMenu[2],"Quitter");

}

void ControllerIU::checkButtonMenu(float x, float y) {
    for (auto & button: buttonMenu){
        button.setSelected(button.isSelected(x, y));
    }
}


/*Ici je verifie seulement que le pointeur de souris est sur le bouton
 * si tel est le cas la variable selected passe a true  */
void ControllerIU::checkButton(float x, float y) {
    size_t size = button.size();
    if(select){
        button.back()->setCoor(x-button[button.size()-1]->getSize()[0]/2,y-button[button.size()-1]->getSize()[0]/2);
    }
    if(spell){
        spell->setCoor(x-spell->getSize()[0]/4,y-spell->getSize()[0]/4);
    }
    for (size_t i = 0; i < size ; i++){
        button[i]->setSelected(button[i]->isSelected(x,y));

        /* Si on a selectionnée un bouton on affiche
         * son doublon de façon transparente
         */
        if(select && i == size-1){
            iuGraphic.changeAlpha(0.5f);
        }else{
            iuGraphic.changeAlpha(1.0f);
        }
    }
    for (auto &spell: spellButton){
        spell.setSelected(spell.isSelected(x, y));
    }



}
// retire le bouton transparent permettant de placer la tour
void ControllerIU::deleteGhost() {
    if(select){
        button.pop_back();
        select = false;
    }
    if(spell){
        spell.reset(nullptr);
    }

}




/*Ici on fait les actions
 * je verifie simplement la variable selected des boutons et
 * lorsque celle ci est a true j'appelle la fonction stockée dans le boutons correspondant*/
void ControllerIU::click() {
    if(spell){
        spell->useButton();
    }
    for(auto &spell :spellButton){
        if(spell.getSelected()){
            spell.useButton();
            break;
        }
    }
    for (auto &iter : button){
        if(iter->getSelected()){
            iter->useButton();
            break;

        }
    }

}

void ControllerIU::clickMenu() {

    for(auto & button : buttonMenu){

        if(button.getSelected()) {
            button.useButton();
            button.setSelected(false);
            break;
        }
    }

}

void ControllerIU::clickEnd() {
    if(end.getSelected()){
        end.useButton();
    }
}


void ControllerIU::setColorAccel() {
    if(colorAccel[0] >= 0.5){
        colorAccel = glm::vec3(0.,0.,0.);
    }else{
        colorAccel = glm::vec3(0.5,0.5,0.5);
    }

}

void ControllerIU::decreaseCooldown() {
    for(auto & spell  : spellButton){
        if(spell.isActive()){
            spell.decreaseCooldown();
        }
    }

}


/*
 * Pour gérer les 2 clics c'est siiiiiiiiiimple non je déconne j'ai passer 3h dessus
 * en gros on met la même lambda dans tous les boutons
 * mais si regis->useSpell
 * case 0 : le sort à echouer car plus de mana
 * case 1 : le sort est lancé
 * case 2 : le sort ne s'est pas lancé car 2 clicks
 * donc un bouton en lui donnant l'autre lambda qui elle appelera la fonction avec les bonnes coordonnées
 *
 *
 * cf ZoneEffect*/
void ControllerIU::createSpell(std::string key, int cooldown, float range) {
    spellButton.emplace_back(widthWindow-(2.5+spellButton.size())*(widthButton*1.2),heightWindow-(widthWindow/16), widthButton, widthButton, utils::element::NEUTRAL, [this, key, cooldown, range] {
        SpellButton &button  = spellButton.back();
        int resu = 1;
        if (!spell){
            resu = regis->useSpell(key);
            if(resu == 2){

                spell = std::make_unique<SpellButton>(button.getPosition().x, button.getPosition().y, range,
                                                      button.getType(), [this,key, cooldown, range, &button]{
                            button.useButton();
                            bool oks = (bool)regis->useSpell(key,(int) spell->getPosition().x,(int) spell->getPosition().y);
                            spell.reset(nullptr);

                            return oks;
                        }, key);
                resu = 0;
            }
        }
        return resu;
    }, cooldown, key);

}

bool ControllerIU::isSomethingSelected(int x, int y){
    bool resu = false;
    if(spell)
        resu = true;
    for(auto &iter1 : spellButton){
        if(iter1.getSelected())
            resu = true;
    }
    for(auto &iter2 : button){
        if(iter2->getSelected())
            resu = true;
    }
    return resu;
}

void ControllerIU::findMiddle(Button &button) {
    button.setCoor(button.getPosition().x-(button.getSize().x/2), button.getPosition().y-button.getSize().y/2);

}

//permet de donner un effet de type ressort lors du déroulement du menu pause
void ControllerIU::animMenu() {
    const int posXMenu = widthWindow/2 - 50;
    const float length = heightWindow/2;
    constexpr float rigidity = 0.01;
    constexpr float  poids = 0.1 ;
    constexpr float weight = 1.1;
    static float  vit = 15.2;
    float posy = buttonMenu[0].getPosition().y;
    float strength =  (-rigidity*(posy-length));

    vit += strength+poids;
    vit /= weight;
    if(vit - (int)vit < 0.2){
        vit = (int)vit;
    }
    posy = buttonMenu[0].getPosition().y+vit;
    for(int i = 0; i<buttonMenu.size(); i++){

        buttonMenu[i].setCoor(posXMenu, posy+i*50);
        findMiddle(buttonMenu[i]);
    }

}

void ControllerIU::resetPosMenu() {

    for(auto & button : buttonMenu){
        button.setY(0);
    }
}

void ControllerIU::displayEnd(utils::state state) {
    iuGraphic.displayEnd(state, end);

}

void ControllerIU::checkEnd(float x, float y) {
    end.setSelected(end.isSelected(x, y));
}


void ControllerIU::callRegisCreateTower(utils::element type) {
    regis->createTower(type);
}




