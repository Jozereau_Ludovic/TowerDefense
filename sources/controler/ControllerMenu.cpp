//
// Created by stiven on 21/04/16.
//

#include <game.hpp>
#include <iostream>
#include "controller/ControllerMenu.hpp"


ControllerMenu::ControllerMenu():ControllerIU(0,0) {}

ControllerMenu::ControllerMenu(int width, int height, SDL_Window *window, bool *run) : ControllerIU(width, height), graphics(width, height) {
    int widthButton = width / 10;
    int heightButton = height / 20;
    button.push_back(std::make_unique<Button>(width / 2, height / 4, widthButton/1.5, heightButton, utils::element::NEUTRAL,
                        [this, window,run] {
                            next:
                            if(play(widthWindow, heightWindow, window, run)){
                                goto next;
                            }
                                },true));
    button.push_back(std::make_unique<Button>(width / 2, 2 * heightButton + height / 4, widthButton, heightButton, utils::element::NEUTRAL, [run] {*run = false; },true));
    for(auto & oneButton : button){
        findMiddle(*oneButton);
    }

}


void ControllerMenu::display() {
    graphics.displayBackground(0);
    iuGraphic.changeColor(glm::vec3(0.,0.,0.));
    iuGraphic.displayTextButton(*button[0],"Jouer");
    iuGraphic.displayTextButton(*button[1],"Quitter");

}





