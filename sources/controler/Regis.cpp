//
// Created by stiven on 12/03/16.
//

#include "controller/Regis.hpp"
#include <iostream>
#include "controller/Regis.hpp"
#include <engine/spell/OneClicMiroir.hpp>
#include <engine/spell/OneClicEffect.h>
#include <engine/buff/Slower.h>
#include <engine/spell/Meteor.hpp>
#include <engine/spell/Glue.hpp>
#include <random>
#include <engine/spell/ZoneEffect.h>
#include "engine/buff/BuffTower.hpp"
#include "engine/spell/OneClicEffect.h"
#include "utils.hpp"

Regis::Regis(): path("../resources/path/lvl1"),
                airPath("../resources/path/lvl1"),
                game(path,airPath),
                controlerGraph(0, 0, &path, *this),
                iu(0, 0, 0, this, controlerGraph.getGraphics()),
                controlerMenuTower(0,0,0,*this),
                frame(0),
                sizeWindow(glm::vec2(0, 0)) {

}

Regis::Regis(int widthWindow, int heightWindow, SDL_Window *window) : path("../resources/path/lvl1"),
                                                                      airPath("../resources/path/lvl1"),
                                                                      game(path,airPath),
                                                                      controlerGraph(widthWindow, heightWindow, &path, *this),
                                                                      iu(widthWindow, heightWindow, 4, this, controlerGraph.getGraphics()),
                                                                      controlerMenuTower(widthWindow, heightWindow, 0, *this) ,
                                                                      frame(0),
                                                                      sizeWindow(glm::vec2(widthWindow, heightWindow)) {


    int cooldown = 500;
    addSpell("miroir",std::make_unique<OneClicMiroir>(10, cooldown, utils::element::FIRE,50,path),cooldown);
    addSpell("earthQuake", std::make_unique<OneClicEffect>(10, cooldown, utils::element::EARTH, 50, std::vector<std::shared_ptr<Buff>>{std::make_shared<Poison>(60*5,2,30)}),cooldown);
    cooldown = 600;
    addSpell("meteore",std::make_unique<Meteor>(20,cooldown,utils::element::FIRE,10,std::vector<std::shared_ptr<Buff>>{},30,path),cooldown);
    addSpell("glue",std::make_unique<Glue>(20,cooldown,utils::element::EARTH,0,std::vector<std::shared_ptr<Buff>>{std::make_shared<Slower>(2,80)},7,path,(int)utils::element::NEUTRAL),cooldown);
    cooldown = 1000;

    addSpell("freeze",std::make_unique<OneClicEffect>(10, cooldown, utils::element::WATER, 0, std::vector<std::shared_ptr<Buff>>{std::make_shared<Slower>(10000,80)}),cooldown);

    addSpell("upgradeTower", std::make_unique<OneClicEffect>(10, cooldown, utils::element::AIR, 50, std::vector<std::shared_ptr<Buff>>{std::make_shared<BuffTower>(600, utils::element::NEUTRAL, glm::vec3{1,0,0})}, true ), cooldown);
    addSpell("light", std::make_unique<ZoneEffect>(10, cooldown, utils::element::NEUTRAL, 50, std::vector<std::shared_ptr<Buff>>{}, 100), cooldown);

    spellEffect["earthQuake"] = DisplaySpell( 4 , 0.8f, [this](bool raz){earthQuake(raz);});
    spellEffect["freeze"] = DisplaySpell(1, 80.0f/60, [this](bool raz){freeze(raz);});
    spellEffect["upgradeTower"] = DisplaySpell(1, 10, [this](bool raz){upTower(raz);});
    struct {
        int size = 0;
        glm::vec2 pos{utils::widthWindow/2,utils::heightWindow/2};
        glm::vec2 scale{utils::widthWindow,50};
        int chargeMax = 100;
        int chargeAct = 0;
        int getHp(){return chargeAct;}
        int getHpMax(){return chargeMax;}
        glm::vec2 getPosition(){return pos;}
        int getSize(){return size;}
    }load;
    // ceci est l'écran de chargement
    for ( float i = 0.0f; i < path.getTotalLength(); i+=path.getPas()){
        load.chargeAct = path.discretPath(i);
        int j = (int)i;
        constexpr int pas = 40;
        if(!(j%pas)) {
            glClear(GL_COLOR_BUFFER_BIT);
            controlerGraph.displayLoad(load, load.scale);
            SDL_GL_SwapWindow(window);
        }

    }

    path.addFinalPoint();
    controlerGraph.discretPath();
    controlerGraph.changeScale(1,1);

}

void Regis::displayAll() {

    // controlerGraph.displayT(game.getTower());
    controlerGraph.displayBackground();

    controlerGraph.displayPath();

    controlerGraph.displayProjectile(game.getProjectile(), true);
    controlerGraph.displayEnemy(game.getEnemy());
    if(!(frame%game.getChangeSprite()) && run) {
        controlerGraph.spriteEnemy(game.getEnemy());

    }
    controlerGraph.displayTower(game.getTower());
    if(controlerGraph.getSelectedTower() != nullptr) {
        controlerMenuTower.display();
    }
    controlerGraph.displayProjectile(game.getProjectile());
    glBlendFunc(GL_SRC_ALPHA,GL_ONE);
    libgl::getError();


    // possiblement inutile
    //if(towerBeingCreated != nullptr)
    //   controlerGraph.displayOneRange(towerBeingCreated.get());

    controlerGraph.displayRangeOnClick(game.getTower());
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);




    //application des effets graphique des sorts
    if(run) {
        for (auto &effect : spellActive) {
            if (effect.second) {
                spellEffect[effect.first].apply();

            }

        }
    }
    displayAoe();
    iu.display(game.getStat(), game.getCooldownBeforeWave(), game.getCooldownMax(), isTowerOnPath);

    if(stateOfGame != utils::state::CONTINUE){

        iu.displayEnd(stateOfGame);

    }
    if(!run){
        controlerGraph.displayBackground(1);
        iu.animMenu();
        iu.displayMenu();

    }

}

bool Regis::play(int x, int y) {
    int numberDeath;
    const std::vector<glm::vec2>& way = path.getPath();
    if(run ) {
        for( int i =0; i<game.getAccelGame();i++) {
            if (stateOfGame == utils::state::CONTINUE)
                iu.checkButton(x, y);
            else
                iu.checkEnd(x, y);

            numberDeath = game.play(stateOfGame);
            iu.decreaseCooldown();
            controlerGraph.checkSelection(game.getTower(), x, y);
            for (auto &p:way) {
                isTowerOnPath = (glm::length(p - glm::vec2(x, y+12.5f)) <= 32.5f) || game.testTowerOnTower(glm::vec2(x,y));
                if (isTowerOnPath)
                    break;

            }
        }
        if(numberDeath){
            soundDeath();
        }
    }else{
        iu.checkButtonMenu(x, y);
    }
    return stope;

}

void Regis::clickMouse(int x, int y, Uint8 typeButton) {
    if(run) {
        coorMouse = glm::vec2(x, y);
        if (typeButton == SDL_BUTTON_LEFT) {

            // effectue les actions click, en fonction de leur prio
            prioriteClick(x, y);
            if(stateOfGame != utils::state::CONTINUE) {
                iu.clickEnd();
            }

        }
        else if (typeButton == SDL_BUTTON_RIGHT) {
            iu.deleteGhost();
        }

    }else{
        iu.clickMenu();
    }
}


/**
 * Traite la priorite entre les click sur l'IU, les boutons des tours et les tours
 *
 * IU > bouton menuTower > tour
 */
void Regis::prioriteClick(int x, int y){

    // -- selection d'un bouton de l'iu
    if(iu.isSomethingSelected(x, y)){
        if(controlerGraph.getSelectedTower() != nullptr){
            controlerGraph.getSelectedTower()->setSelected(false);      // deselection de tour si besoi
            suppButtonsMenuTower();                                     // suppression de bouton menuTower
        }

        iu.click();                 // action click du ControllerIU
    }
    else{

        // -- selection d'un bouton menuTower
        if(controlerMenuTower.isAButtonSelected(x, y)){
            if (controlerGraph.getSelectedTower() != nullptr) {
                controlerMenuTower.click(*(controlerGraph.getSelectedTower()), x, y);
            }
        }
        else{

            // -- selection d'une tour
            if(controlerGraph.isATowerSelected(game.getTower(), x, y)){
                controlerGraph.click(game.getTower(), x, y);
            }
        }
    }
}

void Regis::incrementFrame() {
    frame++;

}

/*
 * Création des tours au click sur la map (apres selection dans l'iu)
 *
 * controle position : marges exterieures, pour que tous les boutons de la tour soit toujours visible.
 *
 */

void Regis::createTower(utils::element type) {
    constexpr int margeGauche = 55;
    constexpr int margeDroite = 95;
    constexpr int margeHaut = 15;
    constexpr int margeBas = 135;

    if(coorMouse[0] < (sizeWindow[0]*(1-(1.0f/16.0f))) - margeDroite
       && coorMouse[0] > margeGauche
       && coorMouse[1] > (sizeWindow[1]/16 + margeHaut)
       && coorMouse[1] < sizeWindow[1] - margeBas)
        if (!isTowerOnPath) {
            if (game.createTower(coorMouse, type)) {
                iu.deleteGhost();
            }
        }





}

void Regis::changeAcceleration() {
    game.changeAcceleration();
}

void Regis::pause() {
    run = !run;
    iu.resetPosMenu();

}


int Regis::useSpell(std::string key, int x, int y) {
    int resu = game.useSpell(key, x, y);
    spellActive[key] = (bool)resu;
    return resu;
}

void Regis::addSpell(std::string key, std::unique_ptr<Spell> newSpell, int cooldown) {
    iu.createSpell(key, cooldown, newSpell->getRange());
    game.addSpell(key,std::move(newSpell));
    spellActive[key] = false;

}


void Regis::earthQuake(bool raz) {
    if(raz){
        controlerGraph.changeScale(1,1);
        spellActive["earthQuake"] = false;
    }else{
        std::random_device rande;
        std::mt19937 generate(rande());
        std::uniform_real_distribution<float> distribution(1.02,1.05);
        controlerGraph.changeScale(distribution(generate),distribution(generate));

    }
}
void Regis::freeze(bool raz){
    controlerGraph.displayFreeze(80);
    if(raz){
        spellActive["freeze"] = false;
        controlerGraph.razFreeze();
    }
}

void Regis::upTower(bool raz) {

    controlerGraph.displayUpTower(game.getTower());
    if(raz){
        spellActive["upgradeTower"] = false;
    }


}


/**
 * Action bouton menuTower
 *
 * Suppression de la tour
 */
void Regis::suppTower(){
    game.suppTower(controlerGraph.getSelectedTower());
    controlerMenuTower.deletButtons();
}


/**
 * Action bouton menuTower
 *
 * Level up de la tour
 */
void Regis::upgradeTower(){
    controlerGraph.getSelectedTower()->upgrade();
}


/**
 * Creation des boutons d'une tour
 */
void Regis::creatButtonsMenuTower(int x, int y){
    controlerMenuTower.creatButtons(x, y);
}


/**
 * Suppression des boutons d'une tour
 */
void Regis::suppButtonsMenuTower(){
    controlerMenuTower.deletButtons();
}

void Regis::cheat() {
    if(run)
        game.cheat();

}

void Regis::soundDeath() {
    static int plop = 0;
    if(!plop) {
        utils::sound.play("mort");

    }


}


void Regis::stop(bool next) {
    stope = !stope;
    this->next = next;

}


void Regis::setTowerBeingCreated(std::unique_ptr <Tower> tower){
    towerBeingCreated = std::move(tower);
}

void Regis::launch() {
    game.launch();

}

void Regis::displayAoe() {
    controlerGraph.displayOndeDeChoc(game.getAoe());
    game.changeAoe();

}

bool Regis::getNext() {
    return next;
}





