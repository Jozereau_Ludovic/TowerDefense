//
// Created by stiven on 08/03/16.
//

#include "GameState.hpp"
#include <glm/glm.hpp>
#include "engine/tower/Tower.hpp"
#include "engine/tower/Fire.hpp"
#include "engine/tower/Earth.hpp"
#include "engine/tower/Water.hpp"
#include "engine/tower/Wind.hpp"
#include "engine/enemy/EnemyWave.hpp"
#include <memory>
#include <iostream>
#include <engine/spell/Spell.h>
#include <map>
#include <engine/spell/OneClicMiroir.hpp>
#include "engine/projectile/Miroir.hpp"
#include <vector>
#include <engine/spell/Meteor.hpp>
#include <engine/spell/OneClicEffect.h>
#include <engine/spell/OneClicEffect.h>
#include <engine/buff/Slower.h>
#include <SDL2/SDL_video.h>



GameState::GameState(Path &path, Path &airPath) : changeSprite(10), path(path), airPath(airPath),controllSpell(enemies, towers, projectile) {
    wave.waveCreation();


}
void GameState::addSpell(std::string key,std::unique_ptr<Spell> newSpell ){
    controllSpell.addSpell(key,std::move(newSpell));
}

//carac contient posX, posY, type de tour
// creer la tour si cela est possible
//renvoie true si cela est possible
int GameState::createTower(glm::vec2 carac, utils::element type) {
    //on rentre dans la condition si la tour que l'on veut creer a un type different de neutre


    bool retour = false;
    if (type != utils::element::NEUTRAL && carac[0]) {
        bool ok = true;
        int i = 0;
        size_t sizeTower = towers.size();
        // on vérifie que la tour ne vas pas être placer par dessus une autre
        while (ok && i < sizeTower) {
            if (carac.x > towers[i]->getPosition().x - widthTower && carac.x < towers[i]->getPosition().x + widthTower
                && carac.y < towers[i]->getPosition().y + heightTower &&
                carac.y > towers[i]->getPosition().y - heightTower) {
                ok = false;

            }
            i++;
        }

        if (ok) {
            retour = true;
            std::unique_ptr<Tower> ptr;
            switch (type) {
                case utils::element::FIRE:
                    ptr = std::make_unique<Fire>(glm::vec2(carac[0], carac[1]));
                    break;
                case utils::element::EARTH:
                    ptr = std::make_unique<Earth>(glm::vec2(carac[0], carac[1]));
                    break;
                case utils::element::AIR:
                    ptr = std::make_unique<Wind>(glm::vec2(carac[0], carac[1]));
                    break;
                case utils::element::WATER:
                    ptr = std::make_unique<Water>(glm::vec2(carac[0], carac[1]));
                    break;
                default:
                    break;
            }
            // c'est ici qu'on paie les tours
            if (ptr->getPrice() <= statPlayer.gold) {
                statPlayer.gold -= ptr->getPrice();
                towers.push_back(std::move(ptr));

            }
            frameReel.push_back(0);
        }
    }
    return retour;
}

bool GameState::testTowerOnTower(glm::vec2 carac) {
    bool ok = false;
    int i = 0;
    size_t sizeTower = towers.size();
    while (!ok && i < sizeTower) {
        if (carac.x > towers[i]->getPosition().x - widthTower && carac.x < towers[i]->getPosition().x + widthTower
            && carac.y < towers[i]->getPosition().y + heightTower &&
            carac.y > towers[i]->getPosition().y - heightTower) {
            ok = true;

        }
        i++;
    }
    return ok;
}


//TODO: à optimiser sa mère
//déroulement du jeu
int GameState::play(utils::state &state) {
    int numberDeath = 0;


        frameJeu++;
        if (acceleration)
            frameAccel++;



        if(launchWave) {
            std::vector<std::unique_ptr<Enemy>> newEnemies{wave.waveApplication()};
            for (int j = 0; j < newEnemies.size(); ++j) {
                enemies.push_back(std::move(newEnemies.front()));
                newEnemies.erase(newEnemies.begin());
            }

        }
        // trie les ennemies
        std::sort(enemies.begin(),enemies.end(),[](const std::unique_ptr<Enemy> & a, const std::unique_ptr<Enemy> &b){
            return *a < *b;});
        //  Vérification collisions
        for (auto &enemy : enemies) {
            enemy->buffUpdate();
            for (size_t i = 0; i < projectile.size(); i++) {

                if (enemy->collision(*projectile[i])) {
                    // enemy->takeDamage((int)projectile[i]->getElement(),projectile[i]->getDamage());
                    if(enemy->IsHit(*projectile[i]))
                        projectile[i]->decrementEnemy();

                    if (!(projectile[i]->usable())) {
                        projectile.erase(projectile.begin() + i);
                        i--;
                    }
                }
            }
        }


        //    Vérification mort enemy
        for (size_t i = 0; i < enemies.size(); i++) {
            if (enemies[i]->isDead() || enemies[i]->isArrived(path)) {
                if (enemies[i]->isArrived(path)) {
                    if(statPlayer.hp > 0)
                        statPlayer.hp--;
                } else {
                    statPlayer.gold += 10;
                    numberDeath++;                }
                enemies.erase(enemies.begin() + i);
                i--;

            }
        }

        //    Vérification game over
        if (statPlayer.hp <= 0 ) {
            state = utils::state::LOSE;

		}
		
		if (enemies.size() == 0 && wave.remainingEnemies() == 0 && state == utils::state::CONTINUE) {
            state = utils::state::WIN;

		}

        //   les enemies avancent
        moveEnemies();
        //   Avance projectiles
        moveProj();

        //TODO: probleme de hit box
        //   Détection enemies
        for (size_t i = 0; i < towers.size(); i++) {
            int j = 0;
            towers[i]->updateBuffTower();
            //std::cout << towers[i]->getRange() << std::endl;
            while (j < enemies.size()) {
                if (towers[i]->detect(enemies[j].get())) {
                    break;
                }
                j++;
            }

            //   Tir projectile
            if (towers[i]->getTarget() != nullptr) {
                float frequency = 60 / (towers[i]->getFrequency() * accelGame);

                // permet de gerer le création de projectile pour une frequence supérieur a  1 proj/frame
                if (frequency < 1) {
                    // accel a 10 est beaucoup trop eleve donc ceci ne sert a rien .... :'(
                    while (frameReel[i] * frequency <= frameAccel) {
                        frameReel[i]++;
                        projectile.push_back(towers[i]->shoot(path, 1));
                        float pas = (frameAccel) - (frameReel[i] * frequency);
                        projectile.back()->move(1 * pas);
                    }
                } else {

                    if (!(frameJeu % ((int) frequency))) {
                        projectile.push_back(towers[i]->shoot(path, 1));
                    }
                }
                towers[i]->reset();
            }
        }


    return numberDeath;
}

void GameState::changeAcceleration() {
    if (!acceleration) {
        changeSprite -= accelSprite;
        accelGame *= valueAccel;


    } else {
        changeSprite += accelSprite;
        accelGame /= valueAccel;

    }
    acceleration = !acceleration;


}

void GameState::moveEnemies() {
    int i = -1;
    for (auto &enemy : enemies) {
        i++;
        if (enemy->isFlying()){
            enemy->move(airPath, accelGame, enemies, i);
        }
        else {
            enemy->move(path, accelGame, enemies, i);
        }
    }
}


const StatPlayer &GameState::getStat() const  {
    return statPlayer;
}

int GameState::useSpell(std::string key, int x, int y) {
    int success = 0;
    if(controllSpell.getMana(key)<=statPlayer.mana) {
        if(controllSpell.applySpell(key, x, y)) {
            statPlayer.mana -= controllSpell.getMana(key);
            success = 1;
        }else{
            success = 2;
        }
    }
    return success;
}

void GameState::moveProj() {
    for (auto it=projectile.begin(); it != projectile.end();) {
        (*it)->move(accelGame);
        if ((*it)->isnull()){
            if(!((*it)->getIsDetectedByEnemy())) {
                (*it)->aoe(enemies);
                aoe.push_back(std::move(*it));
            }
           projectile.erase(it);
        }
        else {
            it++;
        }
    }
}

std::vector<std::unique_ptr<Tower> > &GameState::getTower() {
    return towers;
}

std::vector<std::unique_ptr<Enemy> > &GameState::getEnemy() {
    return enemies;
}

std::vector<std::unique_ptr<Projectile> > &GameState::getProjectile() {
    return projectile;
}
const int GameState::getChangeSprite() const {
    return changeSprite;
}


/**
 * Action du bouton de suppression, du menu d'une tour
 */
void GameState::suppTower(Tower *tower){
    int i = 0;
    for(auto &iter : towers){
        if(iter.get() == tower){
            towers.erase(towers.begin() + i);
            statPlayer.gold += 30;
        }
        i++;
    }
}


void GameState::cheat() {
    statPlayer.gold+=5000;
    statPlayer.hp+=5000;
    statPlayer.mana+=5000;


}

void GameState::launch() {
    launchWave = true;
    wave.sendWave();
}

void GameState::changeAoe() {
    for(auto it = aoe.begin(); it!= aoe.end();){
        if((*it)->getSize().x < 100){
            (*it)->setSize(glm::vec2((*it)->getSize().x+glm::vec2(10.0)));
            it++;
        }else{
            aoe.erase(it);

        }

    }

}


const int GameState::getCooldownBeforeWave() {
    int delay = wave.getCurentDelay();
    if(!wave.getIsEndWave()){
        delay = wave.getWaitingForWave();
    }
    return delay;
}

const int GameState::getCooldownMax() {
    return wave.getWaitingForWave();
}


