//
// Created by jozereau on 21/03/16.
//

#include "engine/map/Cloud.hpp"

Cloud::Cloud(glm::vec2 pos, utils::element type, Object * object, float modifier) :
        Neutral(pos, modifier, type, object) {
    type = utils::element::AIR;
}

void Cloud::apply(Tower * tower) {
    if (tower->getType() == utils::element::EARTH) {
        tower->setRange(tower->getRange()*(1 - modifier));
        tower->setFrequency(tower->getFrequency()*(1 - modifier));
        tower->setDamage(tower->getDamage()*(1 - modifier));
    }

    if (tower->getType() == utils::element::AIR) {
        tower->setRange(tower->getRange()*(1 + modifier));
        tower->setFrequency(tower->getFrequency()*(1 + modifier));
        tower->setDamage(tower->getDamage()*(1 + modifier));
    }
}