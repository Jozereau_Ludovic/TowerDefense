//
// Created by jozereau on 21/03/16.
//

#include "engine/map/Neutral.hpp"
#include "engine/tower/Tower.hpp"

Neutral::Neutral(glm::vec2 pos, float modifier, utils::element type, Object * object) :
        pos(pos), modifier(modifier), type(type), object(object) {
    type = utils::element::NEUTRAL;
}

const glm::vec2 Neutral::getPosition() {
    return pos;
}

const float Neutral::getModifier() {
    return modifier;
}

const utils::element Neutral::getType() {
    return type;
}

Object * Neutral::getObject() {
    return object;
}

void Neutral::destruct() {
    object = nullptr;
}