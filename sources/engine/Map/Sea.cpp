//
// Created by jozereau on 21/03/16.
//

#include "engine/map/Sea.hpp"
#include "utils.hpp"

Sea::Sea(glm::vec2 pos, utils::element type, Object * object, float modifier) :
        Neutral(pos, modifier, type, object) {
    type = utils::element::WATER;
}

void Sea::apply(Tower * tower) {
    if (tower->getType() == utils::element::FIRE) {
        tower->setRange(tower->getRange()*(1 - modifier));
        tower->setFrequency(tower->getFrequency()*(1 - modifier));
        tower->setDamage(tower->getDamage()*(1 - modifier));
    }

    if (tower->getType() == utils::element::WATER) {
        tower->setRange(tower->getRange()*(1 + modifier));
        tower->setFrequency(tower->getFrequency()*(1 + modifier));
        tower->setDamage(tower->getDamage()*(1 + modifier));
    }
}