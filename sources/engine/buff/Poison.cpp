//
// Created by hole on 06/03/16.
//


#include "engine/buff/Poison.h"
#include <iostream>

void Poison::applyBuff(Enemy &enemy) {
    if (nextOccur==0){
        enemy.takeDamage(elem, (float)efficiency);
        nextOccur = frequency;
    }
    else{
        nextOccur--;
    }
}

Poison::Poison():Buff() {

}

Poison::Poison(int duration, int quantity, int frequency):Buff(quantity,duration,utils::element::FIRE,frequency,frequency){

}

void Poison::operator=(Poison &poison){
    efficiency = poison.efficiency;
    frequency = poison.frequency;
    nextOccur = poison.nextOccur;
    duration = poison.duration;
    elem = poison.elem;
}

