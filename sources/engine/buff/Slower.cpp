//
// Created by hole on 06/03/16.
//

#include <iostream>
#include "engine/buff/Slower.h"

void Slower::applyBuff(Enemy &enemy) {
    if (!wasDone) {

        if (enemy.getInitSpeed()-enemy.resistance(elem)*efficiency*enemy.getInitSpeed() < enemy.getSpeed()) {
            enemy.decrementSpeed(efficiency, elem);
            wasDone = true;
        }
    }
    else {

        if (duration==0) {
                enemy.decrementSpeed(0, elem);


        }
    }

}

Slower::Slower():Buff() {

}

Slower::Slower(int efficiency,int duration):Buff(efficiency,duration){

}

void Slower::operator=(Slower &slow){
    efficiency = slow.efficiency;
    wasDone = slow.wasDone;
    duration = slow.duration;
    elem = slow.elem;
}
