//
// Created by diggyhole on 15/02/16.
//

#include "engine/projectile/Projectile.h"
#include "engine/enemy/Enemy.hpp"
#include <iterator>
#include <algorithm> //pour min et max
#include "engine/path/Path.h"

#include <glm/glm.hpp>
#include <iostream>
#include <engine/buff/Poison.h>
#include <engine/buff/Slower.h>
#include <iostream>
#include <cstdlib>
#include <ctime>


int Enemy::compteur = 0;


Enemy::Enemy() {
    progression = 0;
    speed = 1;
    size = 10;
    speed = 1;
    id = compteur;
    compteur++;
    calculEnemyUv();
}

Enemy::Enemy(int hp, float speed, glm::vec2 position, float size, int type, utils::element elem, bool volant, float marge)
        : hp(hp), hpMax(hp), speed(speed), initSpeed(speed), position(position), truePosition(position), progression(0), size(size), type(type), fly(volant), elem(elem) {
    id = compteur;
    compteur++;
    this->marge = (rand()/(float)RAND_MAX)*marge - marge/2;
    calculEnemyUv();

}


bool Enemy::isFlying() {
    return fly;
}


bool Enemy::isDead() {
    return (hp == 0);
}


void Enemy::decremetAllBuff(){  //on a 3 listes de buffs à décrémenter. Gère aussi la suppression
    for(auto it=begin(buffList);it!=end(buffList);){    //buffList : buffs inactifs
        (*it)->decrementDuration();
        if((*it)->isOver()){
            it = buffList.erase(it); //destruction si fin
        }
        else{

            it++;
        }
    }
    for(auto i=begin(activePoison);i!=end(activePoison);){  //activePoison
        (*i)->decrementDuration();
        if((*i)->isOver()){
            activePoison.erase(i);
        }
        else {
            i++;
        }
    }
    for(auto i=begin(activeSlower);i!=end(activeSlower);){  //activeSlower
        (*i)->decrementDuration();
        if((*i)->isOver()){
            (*i)->applyBuff(*this);
            activeSlower.erase(i);

        }
        else {
            i++;
        }
    }
}

void Enemy::changeActive(){
    for (auto i = begin(buffList); i != end(buffList);) {

        std::vector<std::shared_ptr<Buff>> *active;

        if ((*i)->isPoison()) {// le buff est un poison ou un slower ? On compare avec ceux du même type
            active = &activePoison;
        } else {
            active = &activeSlower;
        }

        bool alreadyExists = false; //True si il existe un buff du même type
        bool isDeleted = false;//   Sert pour ne pas avancer l'iterateur si on avance
        for (auto j = (*active).begin(); j != (*active).end();++j) {
            if ((*i)->getElem() == (*j)->getElem()) {   // On cherche un buff du même élément
                alreadyExists = true;
                if (isBetter((*i).get(), (*j).get())) { //échange des buffs si *i est meilleur

                    std::shared_ptr<Buff> temp = *i;
                    buffList.erase(i);
                    buffList.push_back(*j);
                    active->erase(j);
                    active->push_back(temp);

                    isDeleted = true;
                    break;
                }
            }
        }

        if (!alreadyExists) {   //si pas de buff du même élément, insertion direct
            active->push_back(std::move(*i));
            buffList.erase(i);
            isDeleted = true;
        }

        if (!isDeleted) {
            i++;
        }
    }
}


void Enemy::buffUpdate() {

    decremetAllBuff();
    changeActive();
    activeBuff();
}

void Enemy::activeBuff(){   //Active les buffs des deux vector actif
        for(auto &ia : activeSlower) {
            ia->applyBuff(*this);
        }

        for(auto &ia : activePoison) {
       //     int a = ia->getFrequency();
     //       int b = ia->getNext();
//            std::cout << ia->getDuration() << " " << a << "  " << b <<"  "<< ia->getEfficiency() << std::endl;
            ia->applyBuff(*this);
        }

}

bool Enemy::isBetter(Buff *buff1, Buff *buff2) {
    bool resultat = false;
    if(buff1->getEfficiency() > buff2->getEfficiency()) {
        resultat = true;
    }
    return resultat;
}

bool Enemy::IsHit(Projectile &projo) {
    bool touche = false;
    if (collision(projo)) {
        if (projo.getIsDetectedByEnemy()) {
            if (!projo.isAlreadyTouch(id)) {
                utils::sound.play("touche");
                projo.ajoutId(id);
                takeDamage(projo.getElement(), (int) (projo.getDamage()));

                takeBuff(projo.getVbuff());
                touche = true;
            }
        }
    }
    return touche;
}


void Enemy::takeBuff(std::vector <std::shared_ptr <Buff>> buffs){

    for (int i=0; i<buffs.size(); ++i){

        if (buffs[i]->isPoison()==1){
            buffList.push_back(std::make_shared<Poison>());
        }
        else if (buffs[i]->isPoison()==0){
            buffList.push_back(std::make_shared<Slower>());
        }
        *buffList.back() = *buffs[i];
    }
    //std::cout << buffList[0]->getEfficiency()<<std::endl;

}


bool Enemy::collision(Projectile &projo){
    glm::vec2 d = projo.getPosition() - truePosition;
    return(glm::length(d) < size/2);

}

const float Enemy::getProgression() const {
    return progression;
}

glm::vec2 Enemy::getPosition() {
    return truePosition;
}

void Enemy::deplacementTheorique(Path &path, float gameSpeed) {

    glm::vec2 oldPos= position;
    position = (path.getPosition(progression));

    glm::vec2 vecteur = position - oldPos;
    if(vecteur.y>0 && std::abs(vecteur.x)<vecteur.y)    //quart superieur
        orientation = 0;
    else if(vecteur.x>0 && std::abs(vecteur.y)<vecteur.x)  //quart droit
        orientation=3;
    else if(vecteur.y<0 && std::abs(vecteur.x)<(- vecteur.y))  //quart bas
        orientation =2;
    else if(vecteur.x<0 && std::abs(vecteur.y)<(-vecteur.x)) //quart gauche
        orientation =1;

    progression = progression + speed * gameSpeed;


}


void Enemy::takeDamage(utils::element type, float damage){
    hp = std::max(0 , hp - (int)(resistance(type)*damage));

}

void Enemy::decrementSpeed(float quantity,utils::element type) {
    speed = std::max(0.f , (initSpeed - resistance(type)*quantity));
}
float Enemy::resistance(utils::element element) {
    float ratio = 1;
    if (element == utils::element::FIRE) {
        if (elem == utils::element::FIRE)
            ratio = 0.5;
        if (elem == utils::element::AIR)
            ratio = 1.5;
    }
    else {
        if (element == (utils::element::AIR)){
            if (elem == utils::element::WATER)
                ratio = 0.5;
            if (elem == utils::element::FIRE)
                ratio = 1.5;
        }
        else {
            if (element == utils::element::EARTH) {
                if (elem == utils::element::EARTH)
                    ratio = 0.5;
                if (elem == utils::element::AIR)
                    ratio = 1.5;
            }
            else {
                if (element == utils::element::WATER) {
                    if (elem == utils::element::AIR)
                        ratio = 0.5;
                    if (elem == utils::element::EARTH)
                        ratio = 1.5;
                }
            }
        }
    }
    return ratio;
}


float Enemy::getSize() {
    return size;
}
int Enemy::getHpMax() {
    return hpMax;
}
int Enemy::getHp() {
    return hp;
}
const glm::vec2 Enemy::getThPosition() {
    return position;
}


bool Enemy::isArrived(Path &path) {
    return (progression > path.getTotalLength());
}


void Enemy::calculTruePos(std::vector<std::unique_ptr<Enemy>> &vague, Path &path,int k) {
    glm::vec2 reject;
    int j=0;


    for(auto &objet : vague) {
        if (j != k && glm::length(objet->getPosition()-truePosition) < 100) {
            if (objet->getPosition() != truePosition) { //pas de rejet si les objets sont au même endroit mais le leger random à la fin les écartera pour la prochaine frame

                glm::vec2 vecReject = objet->getPosition() - truePosition;
                reject += (vecReject /(glm::length(vecReject))) *   // pour la direction
                           100.0f * //cst G
                           objet->getSize()    //  * M2
                           / (glm::length(vecReject)* glm::length(vecReject));  // div par R²
            }
        }
        j++;

    }

    if (glm::length(reject) > 10.0f)
        reject = (reject/glm::length(reject)) * 10.0f;
    glm::vec2 targ = targetPos(path);
    glm::vec2 totalVect= targ - reject;


    float randx =(rand()/(float)RAND_MAX) / 100;
    float randy =(rand()/(float)RAND_MAX) / 100;
    totalVect.x += randx;
    totalVect.y += randy;


    while (glm::length(truePosition - path.getPosition(progression+1)) < 1){
        progression++;
    }


    truePosition = totalVect;


}


void Enemy::move(Path &path, float gamSpeed, std::vector<std::unique_ptr<Enemy>> &vague,int i) {
    calculTruePos(vague,path,i);
    //truePosition=position;
    deplacementTheorique(path,gamSpeed);

}

glm::vec2 Enemy::targetPos(Path &path) {
    glm::vec2 targ = path.getPosition(progression);
    targ = targ + marge*path.getNormale(progression);
//  targ = targ + path.normale(progression) * marge;
    if(glm::length(targ-truePosition)>10.f) {
        targ -= ((targ - truePosition) / glm::length(targ - truePosition)) * 10.f;
    }
    else{
        targ = truePosition;
    }
   // while (glm::length(truePosition - path.getPosition(progression+1)) < 10){
 //       progression++;
   // }
    return targ;
}



bool operator< (const Enemy &enemy1, const Enemy &enemy2){
    return enemy1.isInfTo(enemy2);
}
const void Enemy::calculEnemyUv() {
    glm::vec2 uvHG;
    constexpr float decaleUvx = 1.0f/5.0f;
    constexpr float decaleUvy = 1.0f/4.0f;
    float x = 0.0f;
    int bit;
    for (int i = 0; i < 4 ; i++) {

        bit = ((int)elem>>i) & 1 ;

        if (bit && i < 4) {
            x = (i+1) * decaleUvx;
            break;
        }


    }
    posInTexture.x = x;
    baseUvx = posInTexture.x;
   posInTexture.y =  decaleUvy*type;
    baseUvy = posInTexture.y;


}

void Enemy::setElem(utils::element element) {
        elem = element;

}

