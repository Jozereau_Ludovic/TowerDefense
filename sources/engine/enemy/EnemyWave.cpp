//
// Created by hole on 01/05/16.
//

#include "engine/projectile/Projectile.h"
#include "engine/enemy/EnemyWave.hpp"
#include <iterator>
#include <algorithm> //pour min et max
#include "engine/path/Path.h"

#include <glm/glm.hpp>
#include <iostream>
#include <engine/buff/Poison.h>
#include <engine/buff/Slower.h>
#include <fstream>

void EnemyWave::waveCreation(){
    int hp,type,delay,enemyNb,endwave;
    float speed,size;
    int elem;
    utils::element elems;
    glm::vec2 pos;
    std::ifstream fichier("../resources/Waves/enemy_waves");
        if(fichier) {
			fichier>>enemyNb;

			while (enemyNb){
				if(!(fichier >> hp >> speed >> pos.x >> pos.y >> size >> type >> delay >> endwave >> elem))
					break;


                switch(elem){
                    case 0:
                        elems = utils::element::NEUTRAL;
                        break;
                    case 1:
                        elems = utils::element::EARTH;
                        break;
                    case 2:
                        elems = utils::element::FIRE;
                        break;
                    case 3:
                        elems = utils::element::WATER;
                        break;
                    default:
                        elems = utils::element::AIR;
                }
                enemyWave.push_back(std::make_unique<Enemy>(hp, speed, pos, size, type, elems));
				//fichier>>delay;
				delays.push_back(delay);
				endwaves.push_back(endwave);
				enemyNb -= 1;
				
        }


		//assert(false);
    }else{
		throw std::runtime_error("fichier ../resources/Waves/enemy_waves non trouvé");
	}
	
}

std::vector<std::unique_ptr<Enemy>> EnemyWave::waveApplication(){
    std::vector<std::unique_ptr<Enemy>> currentEnemies;
    if(!enemyWave.empty()) {
        while (!currentDelay && !enemyWave.empty()) {
            currentDelay = delays.front();
            waitingForWave = currentDelay;
//            std::cout<<currentDelay<<std::endl;
            delays.erase(delays.begin());
            currentEnemies.push_back(std::move(enemyWave.front()));
            enemyWave.erase(enemyWave.begin());
            isEndWave = endwaves.front();
            endwaves.erase(endwaves.begin());
        }
//        std::cout<<isEndWave<<std::endl;
        currentDelay--;
    }
    return currentEnemies;

}


int EnemyWave::remainingEnemies() {
	return enemyWave.size();
}

void EnemyWave::sendWave() {
    currentDelay = 0;
	delays[0]=0;
	isEndWave=0;
}
EnemyWave::EnemyWave(){

}

EnemyWave::EnemyWave(int numWave,int numLevel){

}
