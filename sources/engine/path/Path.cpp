//
// Created by hole on 07/03/16.
//

#include "engine/path/Path.h"
#include "engine/path/SegDroite.h"
#include "engine/path/SegBezier.h"
#include <fstream>
#include <memory>
#include <cmath>
#include <algorithm>
#include <iostream>

glm::vec2 Path::getPositionWithFct(float t){
    assert(!way.empty() && "Pas de segment.");
    int i = 0;
    while(way[i]->getLength()<t) {
        t -= way[i]->getLength();
        i++;
        assert(i < way.size() && "t invalide");
    }
    return way[i]->calculPosReele(t);
}

glm::vec2 Path::getPosition(float t){
    int i = (int)(t/pas);
    return pointsOfPath[i];
}

glm::vec2 Path::getNormale(float t) {
    int i = (int)(t/pas);
    return normaleTab[i];
}


Path::Path() {

    way.push_back(std::make_unique<SegDroite> ());
    totalLength = 0.0f;
}

Path::Path(std::string fileName) {
    std::ifstream streamFile (fileName);
    int type;
    totalLength = 0;
    while (streamFile >> type){
        if (type == 0) {
            glm::vec2 coord1;
            glm::vec2 coord2;
            streamFile >> coord1.x >> coord1.y >> coord2.x >> coord2.y;
            way.push_back(std::make_unique <SegDroite>(coord1, coord2));
            totalLength += glm::distance(coord1,coord2);
        }
        else {
            if (type == 1) {
                glm::vec2 b1;
                glm::vec2 b2;
                glm::vec2 b3;
                glm::vec2 b4;
                streamFile >> b1.x >> b1.y >> b2.x >> b2.y >> b3.x >> b3.y >> b4.x >> b4.y;
                auto bez = std::make_unique <SegBezier>(b1, b2, b3, b4);
                totalLength += bez->longueur();
                way.push_back(std::move(bez));
            }
        }

    }

}


int Path::discretPath(float i) {

    pointsOfPath.push_back(getPositionWithFct(i));

    if (i != 0.0f){
        glm::vec2 prec = pointsOfPath[pointsOfPath.size()-2];
        glm::vec2 cour = pointsOfPath.back();
        cour = cour - prec;
        float tmp = cour.x;
        cour.x = cour.y;
        cour.y = tmp  * -1.0f;
        cour = cour / glm::length(cour);
        normaleTab.push_back(cour);
    }
    else{
        normaleTab.push_back(glm::vec2{0});
    }


    // std::cout << i << "  "<< getTotalLength() << "  " << pas << "    "<< i/getTotalLength()*100<<"%"<< std::endl;
    return  (int)(i/getTotalLength()*100);


}


void Path::addFinalPoint() {
    pointsOfPath.push_back(getPositionWithFct(getTotalLength())); // on pourra donc appeler la valeur du path max
    normaleTab.push_back(glm::vec2{0});

}

