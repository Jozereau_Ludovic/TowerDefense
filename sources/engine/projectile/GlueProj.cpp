//
// Created by stiven on 11/05/16.
//

#include "engine/projectile/GlueProj.hpp"


GlueProj::GlueProj(glm::vec2 pos, glm::vec2 vit, float damage, std::vector<std::shared_ptr<Buff>> &buff,
                   int numberEnemy, utils::element element, int countdown) : Projectile(pos, vit, damage, buff, numberEnemy, element,countdown){
    size = glm::vec2(400);
    isDetectedByEnemy = false;

}


void GlueProj::move(float accelGame) {
    if(size.x > 20 && !isDetectedByEnemy) {
        size-=10;
    }else{
        isDetectedByEnemy = true;
        type = utils::element::GLUE;
        if(size.x < 40)
            size +=3;
        decrementation();
    }

}

