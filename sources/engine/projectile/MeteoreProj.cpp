//
// Created by stiven on 21/05/16.
//


#include <engine/projectile/Projectile.h>
#include <engine/projectile/MeteoreProj.hpp>
#include <random>
#include <engine/buff/Poison.h>
#include <engine/buff/Slower.h>




MeteoreProj::MeteoreProj(glm::vec2 pos,
                         glm::vec2 vit,
                         glm::vec2 size,
                         float damage,
                         std::vector<std::unique_ptr<Buff>> &buff,
                         utils::element element,
                         int countdown) :
        Projectile::Projectile( pos, vit, damage, buff, element, countdown, false){

    this->size = size;

    for (int i = 0; i < buff.size(); ++i) {

        if (buff[i]->isPoison() == 1) {
            vbuff.push_back(std::make_shared<Poison>());
        }
        else if (buff[i]->isPoison() == 0) {
            vbuff.push_back(std::make_shared<Slower>());
        }
        *vbuff.back() = *buff[i];
    }

}


void MeteoreProj::changeRotate() {
    size.y *=-1;
    if(rotation){
        rotation = 0;
    }else{
        rotation = 90;
    }


}

void MeteoreProj::move(float accel) {
    Projectile::move(accel);
    change ++ ;
    if(!(change%timeToChange)){
        changeRotate();
    }
}

