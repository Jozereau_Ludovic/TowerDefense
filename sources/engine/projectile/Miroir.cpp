//
// Created by stiven on 07/04/16.
//
#include <random>
#include "engine/projectile/Miroir.hpp"




Miroir::Miroir(Path &path) : Projectile(10),path(path), progression(path.getTotalLength()) {

    size = glm::vec2(pas*(numberEnemy/10+1));
    type = utils::element::MIRROR;

}
Miroir::Miroir(Path &path,float damage) : Projectile(damage, 10, 0), path(path), progression(path.getTotalLength()) {
    size = glm::vec2(pas*(numberEnemy+1));
    countdown = (int)path.getTotalLength()/(int)(-spd);
    position = (path.getPosition(progression));
    type = utils::element::MIRROR;

}

void Miroir::move(float gameSpeed) {

    position = (path.getPosition(progression));
    progression = progression + spd * gameSpeed;
    //position = glm::vec2(500,500);
    size = glm::vec2(pas*(numberEnemy+1));
    change++;
    if(!(change%timeToChange)){
        changeRotate();
    }
    if(progression <= 0){
        countdown = 0;
    }


}

void Miroir::shorten() {
    size-=pas;
}

void Miroir::changeRotate() {
    std::random_device s;
    std::default_random_engine e2(s());
    std::uniform_real_distribution<float> rotate(0,360);
    rotation = rotate(e2);


}

