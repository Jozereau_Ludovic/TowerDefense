//
// Created by Alexis on 15/02/2016.
//

#include <glm/glm.hpp>
#include "engine/projectile/Projectile.h"
#include "engine/enemy/Enemy.hpp"
#include <memory>
#include <engine/buff/Slower.h>

Projectile::Projectile() { }

Projectile::Projectile(const glm::vec2 pos, glm::vec2 vit, float damage, std::vector <std::shared_ptr <Buff>> &buff, int numberEnemy, utils::element element, int countdown, bool touche) : speed(vit), position(pos), damage(damage), size(20), type(element), numberEnemy(numberEnemy), countdown(countdown), isDetectedByEnemy(touche){
    for (int i=0; i<buff.size(); ++i){

        if (buff[i]->isPoison()==1){
            vbuff.push_back(std::make_shared<Poison>());
        }
        else if (buff[i]->isPoison()==0){
            vbuff.push_back(std::make_shared<Slower>());
        }
        *vbuff.back() = *buff[i];
    }
}
Projectile::Projectile(const glm::vec2 pos, glm::vec2 vit, float damage, std::vector <std::unique_ptr <Buff>> &buff, utils::element element, int countdown, bool touche) : speed(vit), position(pos), damage(damage), size(20), type(element), numberEnemy(1), countdown(countdown), isDetectedByEnemy(touche) {
    for (int i=0; i<buff.size(); ++i){

        if (buff[i]->isPoison()==1){
            vbuff.push_back(std::make_shared<Poison>());
        }
        else if (buff[i]->isPoison()==0){
            vbuff.push_back(std::make_shared<Slower>());
        }
        *vbuff.back() = *buff[i];
    }
}

Projectile::Projectile(const glm::vec2 pos, glm::vec2 vit, float damage, std::vector <std::unique_ptr <Buff>> &buff, int numberEnemys, utils::element element, int countdown, bool touche): Projectile(pos, vit, damage, buff, element, countdown, touche){
    numberEnemy = numberEnemys;
}

Projectile::Projectile(int numberEnney) : damage(15), numberEnemy(numberEnney)  { }
Projectile::Projectile(float damage, int numberEnney, int countdown)
        : damage(damage), numberEnemy(numberEnney), countdown(countdown), isDetectedByEnemy(true)  { }


const glm::vec2 Projectile::getSpeed(){
    return speed;
}

void Projectile::setSpeed(glm::vec2 vit){
    speed = vit;
}

const glm::vec2 Projectile::getPosition(){
    return position;
}

bool Projectile::getIsDetectedByEnemy() {
    return isDetectedByEnemy;
}

void Projectile::move(float accel) {
    position += accel*speed;
    decrementation();
}

const float Projectile::getDamage(){
    return damage;
}

glm::vec2 Projectile::getSize() {
    return size;
}

utils::element Projectile::getElement() const {
    return type;
}

const std::vector <std::shared_ptr <Buff>> &Projectile::getVbuff() const {
    return vbuff;
}


void Projectile::aoe(std::vector<std::unique_ptr<Enemy>> &enemy, int range) {
    isDetectedByEnemy = true;
    for ( auto &i : enemy){
        if (glm::length(i->getPosition()-position) < range){
            i->takeDamage(type ,damage);
        }
    }
}

void Projectile::decrementEnemy() {
    numberEnemy--;
}
bool Projectile::usable(){
    return (bool)numberEnemy;
}

void Projectile::decrementation() {
    countdown-=1;

}

bool Projectile::isnull() {
    return (bool)!countdown;
}

const utils::element Projectile::getType() {
    return type;
}

const std::vector<int> & Projectile::ajoutId(int id) {
    touch.push_back(id);
    return touch;
}

bool Projectile::isAlreadyTouch(int id) {
    bool isTouched = false;
    for ( auto &i : touch){
        if ( id == i){
            isTouched = true;
        }
    }
    return isTouched;
}