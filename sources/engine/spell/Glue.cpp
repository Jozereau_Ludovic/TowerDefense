//
// Created by hole on 10/04/16.
//
#include <cstdlib>
#include "engine/spell/Glue.hpp"
#include "engine/path/Path.h"
#include <vector>
#include "engine/projectile/GlueProj.hpp"

bool Glue::apply(float x, float y, const std::vector<std::unique_ptr<Tower>> &towers,
                 const std::vector<std::unique_ptr<Enemy>> &enemies,
                 std::vector<std::unique_ptr<Projectile>> &projectiles) {



    for(int i = 0 ; i<nbProjo ; i++){
        float randProg =(rand()/(float)RAND_MAX) * path.getTotalLength();
        glm::vec2 Pos = path.getPosition(randProg);

        std::vector<std::unique_ptr<Buff>> buffs {};
        projectiles.push_back(std::make_unique<GlueProj>(Pos,glm::vec2{0, 0},0.0f,buffList, nbEnemy, element,60*5));
    }
    return true;

}

Glue::Glue(int price,
               int coolDown,
           utils::element element,
               float damage,
               std::vector<std::shared_ptr<Buff>> buffList,int nbProjo,Path &path, int nbEnemy):
        Spell(price,coolDown,element, damage,buffList),nbProjo(nbProjo),nbEnemy(nbEnemy),path(path){};




