//
// Created by hole on 10/04/16.
//
#include <cstdlib>
#include <engine/spell/Meteor.hpp>
#include <engine/path/Path.h>
#include <vector>
#include <engine/projectile/MeteoreProj.hpp>

bool Meteor::apply(float x, float y, const std::vector<std::unique_ptr<Tower>> &towers,
                   const std::vector<std::unique_ptr<Enemy>> &enemies,
                   std::vector<std::unique_ptr<Projectile>> &projectiles) {

    for(int i = 0 ; i<nbProjo ; i++){
        float randProg =(rand()/(float)RAND_MAX) * path.getTotalLength();

        float size = (rand()/(float)RAND_MAX)*30.0f +35.0f;
        float damage = size * 0.2f;

        float eloignement = (rand()/(float)RAND_MAX) * 300.0f + 100.0f;
        glm::vec2 speed {-3, 4};
        speed *=(1.0f + (size-30.0f)/50.0f);
        glm::vec2 initPos = path.getPosition(randProg) - eloignement * speed;
        std::vector<std::unique_ptr<Buff>> buffs {};

        projectiles.push_back(std::make_unique<MeteoreProj>(initPos, speed, glm::vec2(size, size), damage, buffs, utils::element::METEORE, eloignement));

    }
    return true;

}

Meteor::Meteor(int price,
               int coolDown,
               utils::element element,
               float damage,
                           std::vector<std::shared_ptr<Buff>> buffList,int nbProjo,Path &path):
            Spell(price,coolDown,element, damage,buffList),nbProjo(nbProjo),path(path){};


