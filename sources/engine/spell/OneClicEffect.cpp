//
// Created by hole on 29/03/16.
//

#include <engine/spell/OneClicEffect.h>
#include <glm/glm.hpp>
#include <iostream>

bool OneClicEffect::apply(float x, float y, const std::vector<std::unique_ptr<Tower>> &towers,
                          const std::vector<std::unique_ptr<Enemy>> &enemies,
                          std::vector<std::unique_ptr<Projectile>> &projectiles) {
    if(!forTower) {
        for (auto &i : enemies) {
            i->takeDamage(element, damage);
            i->takeBuff(buffList);
        }
    }else{
        for (auto &tower : towers){
            tower -> insertBuffTower(buffList);
        }
    }
    return true;
}

OneClicEffect::OneClicEffect(int price, int coolDown, utils::element element, float damage,
                             std::vector<std::shared_ptr<Buff>> buffList, bool forTower) :
        Spell(price,coolDown,element, damage,buffList),forTower(forTower){};



