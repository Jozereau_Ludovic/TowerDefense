//
// Created by hole on 29/03/16.
//

#include <engine/spell/ZoneEffect.h>
#include <glm/glm.hpp>
/*
 * si active cela veut dire que c'est le 2eme appui sur le sort on a donc
 * choisi la possition d'application du sort
 *
 * si x<=0 alors c'est un clic droit donc on annule */
bool ZoneEffect::apply(float x, float y, const std::vector<std::unique_ptr<Tower>> &towers,
                       const std::vector<std::unique_ptr<Enemy>> &enemies,
                       std::vector<std::unique_ptr<Projectile>> &projectiles) {
    if(x <= 0){
        active = false;
    }

    if(active) {

        glm::vec2 position(x, y);
        if (forTower) {
            for (auto &i : towers) {
                if (glm::length(i->getPosition() - position) < range) {
                    //i->takeBuff(buff);//TODO
                }
            }
        }
        else {
            for (auto &i : enemies) {
                if (glm::length(i->getPosition() - position) < range) {
                    i->takeDamage(element, damage);//TODO +type +buff
                }
            }
        }

    }
    active = !active;
    return !active;

}

float ZoneEffect::getRange() {
    return range;
}




ZoneEffect::ZoneEffect(int price, int coolDown, utils::element element, float damage, std::vector<std::shared_ptr<Buff>> buffList, float range):
        Spell(price,coolDown,element, damage,buffList),range(range){};

ZoneEffect::ZoneEffect(int price, int coolDown, utils::element element, float damage, std::vector<std::shared_ptr<Buff>> buffList, float range, bool forTower)
        :Spell(price,coolDown,element, damage,buffList),range(range), forTower(forTower){};