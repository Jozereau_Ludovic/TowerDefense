//
// Created by jozereau on 15/02/16.
//

#include <glm/detail/func_geometric.hpp>
#include <engine/buff/Poison.h>
#include "engine/tower/Fire.hpp"
#include "engine/projectile/Projectile.h"

Fire::Fire() : Tower() {
    type = utils::element::FIRE;
    range = 75.0f;
}

Fire::Fire(glm::vec2 pos) :
        Tower(pos, utils::element::FIRE, 75.0f, 3.0f, 2.5f, 10.0f) {
    buff.push_back(std::make_unique <Poison> (100, 6, 10));
}

std::unique_ptr <Projectile> Fire::shoot(Path &path, float gameSpeed) {
    int time = 0;
    glm::vec2 posFut, direction;
    float prog = target->getProgression();
    bool find = false;

    for (time=0;time<100 && !find;time++) {
        prog += target->getSpeed() * gameSpeed;
        if (prog < path.getTotalLength()){
            posFut = path.getPosition(prog);
            direction = glm::normalize(posFut - pos);

            if (glm::distance(posFut, vit * time * direction+pos) <= target->getSize()/4) {
                find = true;
            }
        }
    }


    return std::make_unique <Projectile> (pos, vit*direction, damage, buff, type, (int)glm::length(posFut-pos)/glm::length(2.0f*direction) );
}