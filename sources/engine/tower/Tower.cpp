//
// Created by jozereau on 15/02/16.
//

#include <glm/detail/func_geometric.hpp>
#include "engine/tower/Tower.hpp"
#include "engine/projectile/Projectile.h"
#include <memory>
#include <engine/buff/Slower.h>


Tower::Tower() : pos{0, 0}, type(utils::element::NEUTRAL), level(0), range(0.0f), damage(0.0f), frequency(0.0f),
                 price(0.0f), target(nullptr),mouseSelection(false) { }

Tower::Tower(glm::vec2 pos, utils::element type, float range, float degats, float frequency, float price) :
        pos(pos), type(type), range(range), initRange(range), damage(degats), initDamage(damage),
        frequency(frequency), initFrequency(frequency), price(price) {
    type = utils::element::NEUTRAL;
    level = 1;
    target = nullptr;
    mouseSelection = false;
}

Tower::Tower(glm::vec2 pos, utils::element type) :
        pos(pos), type(type), range(0.0), initRange(0.0), damage(0.0), initDamage(0.0),
                                                  frequency(0.0), initFrequency(0.0), price(0.0) {
    type = utils::element::NEUTRAL;
    level = 1;
    target = nullptr;
    mouseSelection = false;
}

const glm::vec2 Tower::getPosition() {
    return pos;
}

const utils::element Tower::getType() {
    return type;
}

const int Tower::getLevel() {
    return level;
}

void Tower::upgrade() {
    if (level < 3) {
        ++level;
        damage += 10;
        frequency += 10;
        range += 10;
        setPrice();
    }
}

const float Tower::getRange() {
    return range;
}

void Tower::setRange(float newRange) {
    range = newRange;
}

const float Tower::getDamage() {
    return damage;
}

void Tower::setDamage(float newDamage) {
    damage = newDamage;
}

const float Tower::getFrequency() {
    return frequency;
}

void Tower::setFrequency(float newFrequency) {
    frequency = newFrequency;
}

const float Tower::getPrice() {
    return price;
}

void Tower::setPrice() {
    price += 10;
}

const Enemy * Tower::getTarget() {
    return target;
}

bool Tower::detect(Enemy *enemy) {
    bool find = false;      /* Permet de dire s'il faut continuer de chercher un ennemi */

    if (glm::distance(pos, enemy->getPosition()) <= range) { /* On regarde si l'ennemi est à portée de tir */
        target = enemy;
        find = true;

    }
    else {
        target = nullptr;
    }

    return find;
}

std::unique_ptr <Projectile> Tower::shoot(Path &path, float gameSpeed) {
    int time = 0;
    glm::vec2 posFut, direction;
    float prog = target->getProgression();
    bool find = false;

    for (time=0;time<100 && !find;time++) {
        prog += target->getSpeed() * gameSpeed;
        if (prog < path.getTotalLength()){
            posFut = path.getPosition(prog);
            direction = glm::normalize(posFut - pos);

            if (glm::distance(posFut, 2.0f * time * direction+pos) <= target->getSize()/4) {
                find = true;
            }
        }
    }


    return std::make_unique <Projectile> (pos, 2.0f*direction, damage, buff, type, (int)glm::length(posFut-pos)/glm::length(2.0f*direction) );
}

void Tower::reset() {
    target = nullptr;

}

void Tower::insertBuffTower(std::vector <std::shared_ptr <Buff>> buff) {
    for (int i=0; i<buff.size(); ++i){

        buffTower.push_back(std::make_unique<BuffTower>());
        *buffTower.back() = *buff[i];

    }
    activateBuffTower();



}

void Tower::activateBuffTower() {
    for(auto i = buffTower.begin() ; i != buffTower.end();) {
        if ((*i)->getElem() == type || (*i)->getElem() == utils::element::NEUTRAL) {
            range = range*(1+(*i)->getBuffTower()[0]);
            frequency = frequency*(1+(*i)->getBuffTower()[1]);
            damage = damage*(1+(*i)->getBuffTower()[2]);

        }
        i++;
    }
}

void Tower::desactivateBuffTower() {
    range = initRange;
    frequency = initFrequency;
    damage = initDamage;
}

void Tower::updateBuffTower() {
    for(auto i = buffTower.begin() ; i != buffTower.end();) {
        (*i)->decrementDuration();


        if ((*i)->isOver()) {
            buffTower.erase(i);
            desactivateBuffTower();
        }
        else {
        i++;
        }
    }
}
// ****************** Selection de la tour  ***********************

bool Tower::isMouseSelection(int xMouse, int yMouse){
    return (xMouse > pos.x - 30 && xMouse < pos.x + 30
            && yMouse > pos.y - 30 && yMouse < pos.y + 30);
}

bool Tower::getMouseSelection(){
    return mouseSelection;
}
void Tower::setMouseSelection(bool select){
    mouseSelection = select;
}

bool Tower::getSelected(){
    return selected;
}
void Tower::setSelected(bool select){
    selected = select;
}

bool Tower::isUp() {
    return range!=initRange || frequency !=initFrequency || damage!=initDamage;
}


void Tower::operator=(Tower &tower){
    /*tower.getPosition();
    type;
    level;
    range;
    damage;
    frequency;
    price;
    *target;

    mouseSelection;
    selected;

    *this = tower;*/
}
