//
// Created by jozereau on 15/02/16.
//

#include <glm/detail/func_geometric.hpp>
#include <engine/buff/Slower.h>
#include "engine/tower/Wind.hpp"
#include "engine/projectile/Projectile.h"

Wind::Wind(): Tower() {
    type = utils::element::AIR;
}

Wind::Wind(glm::vec2 pos) :
        Tower(pos, utils::element::AIR, 160.0f, 2.0f, 2.0f, 10.0f) {
    buff.push_back(std::make_unique <Slower> (2, 80));
}

std::unique_ptr <Projectile> Wind::shoot(Path &path, float gameSpeed) {
    int time = 0;
    glm::vec2 posFut, direction;
    float prog = target->getProgression();
    bool find = false;


    for (time=0;time<100 && !find;time++) {
        prog += target->getSpeed() * gameSpeed;
        if (prog < path.getTotalLength()){
            posFut = path.getPosition(prog);
            direction = glm::normalize(posFut - pos);

            if (glm::distance(posFut, vit* time * direction+pos) <= target->getSize()/4) {
                find = true;
            }
        }
    }


    return std::make_unique <Projectile> (pos, vit*direction, damage, buff, type, (int)glm::length(posFut-pos)/glm::length(2.0f*direction) );
}