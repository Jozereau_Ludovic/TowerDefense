//
// Created by stiven on 21/04/16.
//
#include "game.hpp"
#include <GLifier/InitOGL.hpp>
#include <GL/glew.h>
#include <controller/ControllerIU.hpp>
#include "controller/Regis.hpp"

#include <cstdlib>
#include <ctime>

bool play(int width, int height, SDL_Window *window, bool *bigRun) {

    int xMouse = 0, yMouse = 0;
    bool run = true;

    std::srand(std::time(0));


    SDL_Event event;
    int time;

    Regis regis(width, height, window);
    bool retour;

    while (run){
        time = SDL_GetTicks();
        retour = false;
        while(SDL_PollEvent(&event)){
            switch(event.type){
                case SDL_QUIT:
                    run = false;
                    *bigRun = false;
                    break;
                case SDL_KEYUP:
                    switch(event.key.keysym.sym ){
                        case SDLK_ESCAPE:
                            run = false;
                            break;
                        case SDLK_p:
                            regis.pause();
                            break;
                        case SDLK_k:
                            regis.cheat();
                            break;
                    }

                case SDL_MOUSEBUTTONDOWN:
                    regis.clickMouse(xMouse,yMouse,event.button.button);
                    break;
            }
        }
        glClear(GL_COLOR_BUFFER_BIT);
        libgl::getError();
        SDL_GetMouseState(&xMouse, &yMouse);


        regis.displayAll();
        if (!regis.play(xMouse, yMouse)){
            run = false;
            retour = regis.getNext();
        }
        regis.incrementFrame();
        SDL_GL_SwapWindow(window);
        if((SDL_GetTicks()-time)<17)
            SDL_Delay(17-(SDL_GetTicks()-time)); // affichage a 60 images secondes
    }
    return retour;

}