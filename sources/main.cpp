//
// Created by stiven on 08/03/16.
//

#include <GLifier/InitOGL.hpp>
#include <controller/ControllerMenu.hpp>
#include "game.hpp"

int main(){
    constexpr int width  = 1200;
    constexpr int height = 700;
    libgl::InitOGL windowOgl(width, height, "Tower Defense");
    SDL_Event event;
    bool run = true;
    ControllerMenu menu(width, height, windowOgl.getWindow(), &run);
    int mouseX, mouseY;
    while(run){
        SDL_GetMouseState(&mouseX, &mouseY);
        while(SDL_PollEvent(&event)){
            switch(event.type){
                case  SDL_QUIT:
                    run = false;
                    break;
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym ) {
                        case SDLK_ESCAPE:
                            run = false;
                            break;
                    }
                case SDL_MOUSEBUTTONDOWN:
                    if(event.button.button == SDL_BUTTON_LEFT)
                        menu.click();
                    break;
            }

        }
        glClear(GL_COLOR_BUFFER_BIT);
        menu.display();
        menu.checkButton(mouseX,mouseY);
        SDL_GL_SwapWindow(windowOgl.getWindow());
    }
}