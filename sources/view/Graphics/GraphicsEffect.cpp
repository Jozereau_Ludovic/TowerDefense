//
// Created by stiven on 23/04/16.
//

#include <utils.hpp>
#include <iostream>
#include <random>
#include "view/Graphics/GraphicsEffect.hpp"
#include "engine/tower/Tower.hpp"


GraphicsEffect::GraphicsEffect(int width, int height) :Graphics(width, height){


}


void GraphicsEffect::displayUpTower(const std::unique_ptr<Tower> &tower, int use) {
    Graphics::displayRange(tower.get(), 1, 50);
    std::random_device rande;
    std::mt19937 generate(rande());
    constexpr int rangeStar = 40;
    std::uniform_real_distribution<float> distribution(-rangeStar,rangeStar);
    static std::array<float, 5> x ;
    static std::array<float, 5> y ;
    if(!(use%6)){
        for(int i = 0; i <x.size(); i++) {
            x[i] = distribution(rande);
            y[i] = distribution(rande);
        }
    }
    textureStar.bindTexture();
    libgl::getError();
    Range.texture2D = 0;
    libgl::getError();
    Range.uvHG = glm::vec2(0);
    libgl::getError();
    Range.pictureSize = glm::vec2(1);
    libgl::getError();
    transformStruct.scale= glm::vec2(20);
    Range.spell = 2;
    libgl::getError();
    for(int i = 0; i <x.size(); i++) {
        transformStruct.tran = glm::vec2(tower->getPosition() + glm::vec2(x[i]-5, y[i]-5));
        libgl::getError();
        Range.tran = libgl::transform(transformStruct);

        libgl::getError();
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        libgl::getError();
    }

}


void GraphicsEffect ::displayFreeze(float timeExec) {
    vao.bind();
    alpha -= .5*3/timeExec;
    if (alpha > 0.5)
        display.alphas = alpha;
    if(alpha < 0){
        display.alphas = 0.5f  - alpha ;
    }
    libgl::getError();
    textureFreeze.bindTexture();
    libgl::getError();
    display.texture2D = 0;
    libgl::getError();
    display.uvHG = glm::vec2(0);
    libgl::getError();
    display.pictureSize = glm::vec2(1);
    libgl::getError();
    transformStruct.scale= glm::vec2(widthWindow-widthWindow/16.0f,heightWindow-heightWindow/16);
    libgl::getError();
    transformStruct.tran = glm::vec2(0);
    libgl::getError();
    display.tran = libgl::transform(transformStruct);
    libgl::getError();
    display.change = 1.0f;
    libgl::getError();
    display.useTexture = 0;
    glDrawArrays(GL_TRIANGLE_STRIP,0,4);
    libgl::getError();
    display.change = 2.0f;
    libgl::getError();

}


void GraphicsEffect::razAlpha() {
    alpha = 1;

}

void GraphicsEffect::displayOndeDeChoc(std::unique_ptr<Projectile> &proj) {
    displayProjectile(*proj, 2);


}


