//
// Created by lysilia on 04/04/16.
//

#include "view/Graphics/GraphicsMenuTower.h"
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include "utils.hpp"

const float nbPictureLine = 5.0f;     // nombre d'image par ligne
const float nbPictureColumn = 1.0f;    // nombre d'image par colonne

const float widthPictureMenuTower = 1.0f / nbPictureLine;
const float heightPictureMenuTower = 1.0f / nbPictureColumn;
constexpr float decaleUv = 0.25f;

GraphicsMenuTower::GraphicsMenuTower(std::string pathTexture, int widthWindow,
                                       int heightWindow) : texture_menuTower(pathTexture), width(widthWindow), height(heightWindow){

    vao.bind();
    vboRect = libgl::Vbo(0, 3, libgl::mat::rect);
    vboUv = libgl::Vbo(1, 2, libgl::mat::uv);

    // initialisation des uniformes
    program.scale = libgl::normalizedScreenSpace(widthWindow,heightWindow);
    program.bd = glm::vec2(decaleUv, decaleUv*2);


}

/**
 * Affichage des boutons
 */
void GraphicsMenuTower::displayButtons(Button &button, int idBtn) {
    // on affiche les tours de l'IU
    vao.bind();
    texture_menuTower.bindTexture();
 //   program.hg = glm::vec2(0, 0);
 //   program.bd = glm::vec2(1, 1);

    program.hg = glm::vec2(widthPictureMenuTower * idBtn, 0);
    program.bd = glm::vec2(widthPictureMenuTower, heightPictureMenuTower);  // bd = taille


    if(button.getSelected()){
        program.surbrillance = 0.15f;  //mise en surbrillance
    }else{
        program.surbrillance = 0.0f; //non surbrillance
    }

    //transformStruct.tran =glm::vec2(20);
    //transformStruct.scale = glm::vec2(50);

    transformStruct.tran = glm::vec2(button.getPosition().x, button.getPosition().y);
    transformStruct.scale = glm::vec2(button.getSize().x, button.getSize().y);
    program.transformation = libgl::transform(transformStruct);
    program.texture2D = 0;
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    libgl::getError();
}



/* permet de changer la transparence de ce qu'on affiche
 * utilisé pour le placement des tours dans la map*/
void GraphicsMenuTower::changeAlpha(float alpha) {
    program.alpha = alpha;

}


void GraphicsMenuTower::changeColor(glm::vec3 color) {

}

void GraphicsMenuTower::displayRect() {
    vao.bind();
    transformStruct.scale = glm::vec2(1.5*(width/16),height);
    transformStruct.tran = glm::vec2(width-1.5*width/16,0);
    program.transformation = libgl::transform(transformStruct);
    glDrawArrays(GL_TRIANGLE_STRIP,0,4);
    transformStruct.angle = 90;
    transformStruct.scale = glm::vec2((width/16),width);
    transformStruct.tran = glm::vec2(width,height-(width/16));
    program.transformation = libgl::transform(transformStruct,1.0,1.0);
    glDrawArrays(GL_TRIANGLE_STRIP,0,4);
    transformStruct.angle = 0;
}

