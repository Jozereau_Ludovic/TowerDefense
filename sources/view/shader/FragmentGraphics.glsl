#version 330 core


in vec2 coordPixelToDisplay;        // varying  // valeur interpole du pixel (= coord par rapport aux vertices du quad)
in vec2 uv;
uniform sampler2D texture2D;
uniform float change;
uniform float alphas;
uniform int useTexture;

out vec4 colorPixel;

void main() {
    float alpha = texture(texture2D, coordPixelToDisplay).a;
    if(change == 1){
        alpha -= alphas;
    }
    if(useTexture == 0){
        colorPixel = vec4(texture(texture2D, coordPixelToDisplay).rgb,alpha);
   }else if(useTexture == 1){
        const vec4 grey = vec4(0.5);
        colorPixel = grey;
   }else if (useTexture == 2){
        if(length (uv - vec2(0.5, 0.5)) > 0.4 && length (uv - vec2(0.5, 0.5)) < 0.5){
            colorPixel = vec4(1.0, 0, 0, 1.0);
        }else{
                 colorPixel = vec4(0., 0., 0., 0.);
            }
   }

}
