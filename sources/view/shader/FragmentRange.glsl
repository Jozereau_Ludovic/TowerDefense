#version 330 core


in vec2 coordPixelToDisplay;        // varying  // valeur interpole du pixel (= coord par rapport aux vertices du quad)

uniform sampler2D texture2D;
uniform int spell;
uniform int onPath;

out vec4 colorPixel;

void main() {

    if(spell == 0 || spell == 1){
        // etablir la distance par rapport au centre du carre, 1 fragment = 1 pixel (ish)
        vec2 centre = vec2(0.5, 0.5);
        float rayon = 0.5;

        // vec2 newPoint = vec2(coordPixelToDisplay.x - centre.x, coordPixelToDisplay.y - centre.y);
        vec2 vecNewPoint = coordPixelToDisplay - centre;

        float distanceNewPoint = length(vecNewPoint);

        float interpolation1 = smoothstep(0.45, 0.5, distanceNewPoint);
        float interpolation2 = smoothstep(0.5, 0.48, distanceNewPoint);
        float interpolation = interpolation1 * interpolation2;


        // definir la couleur
        if(distanceNewPoint < 0.47){
            interpolation1 = smoothstep(0., 0.5, distanceNewPoint);
            interpolation2 = smoothstep(0.5, 0.45, distanceNewPoint);
            interpolation = interpolation1 * interpolation2;
            colorPixel = vec4(0.2, 0.3, 0.3, interpolation);
        }
        else{
            colorPixel = vec4(0.2, 0.8, 0.8, interpolation);
        }
       if(spell == 1){
            colorPixel = vec4(0.8, 0.8, 0.0, interpolation-0.2);
       }else if (onPath == 1){
            colorPixel = vec4(1., 0.0, 0.0, interpolation-0.2);
       }
   }else{
        colorPixel = texture(texture2D,coordPixelToDisplay);
   }



}
