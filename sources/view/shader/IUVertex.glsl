#version 330 core

layout (location = 0) in vec3 vertices;
layout (location = 1) in vec2 uv;
uniform mat3 scale;
uniform mat3 transformation;
uniform vec2 hg;
uniform vec2 bd;

out vec2 uvFragment;
void main() {
    gl_Position = vec4 (scale*transformation*vertices,1.);
    uvFragment =mix(hg,hg+bd,uv);

}
