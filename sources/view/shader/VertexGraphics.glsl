#version 330 core

// viens du vbo
layout (location = 0) in vec3 vertices;     // sommets du carre
layout (location = 1) in vec2 coordPixel;

// discussion avec le cpp
// meme nom que dans le glGetUniformLocation
uniform mat3 scale;     // pour les deux attributs scale.
uniform mat3 tran;
uniform vec2 uvHG;


uniform vec2 pictureSize;

out vec2 coordPixelToDisplay;
out vec2 uv;

void main() {
    gl_Position = vec4(scale * tran * vertices, 1.);  // 1. facilite mathematique
    coordPixelToDisplay = mix(uvHG, uvHG+pictureSize, coordPixel);
    uv = coordPixel;
}
