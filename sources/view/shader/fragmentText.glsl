#version 330 core
in vec2 uvOut;
out vec4 color;
uniform sampler2D textureSampler;
uniform vec3 colorText;
uniform vec3 couleur;
uniform float alpha;
void main() {

    color = texture(textureSampler,uvOut).rgba+vec4(colorText,-(1-alpha));

    if(color[0] >= 1. && color[3]>0.){
        color = vec4(color.rgb+colorText-couleur,alpha);
    }
}
