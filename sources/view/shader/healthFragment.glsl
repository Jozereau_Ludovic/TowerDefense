#version 330 core
in vec2 uvOut;

out vec4 color ;

uniform int hpMax;
uniform int hp;
vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main() {
    float hpNormalized = float(hp)/hpMax;
    float green = hpNormalized;
    float red = 1-green;
    float blue = 0.;
    vec3 rainbow = hsv2rgb(vec3(uvOut[0],1,1));

    if(uvOut[0] > hpNormalized){
        green = 0.0f;
        red = 0.;
       // rainbow = vec3(0.,0.,0.);
    }
    if(uvOut[0] >0 && uvOut[0]< 0.05 || uvOut[0] >1-0.05 && uvOut[0]<1
    || uvOut[1] >0 && uvOut[1]< 0.15 || uvOut[1] >1-0.15 && uvOut[1]<1){
        green = 1.0f;
        red = 1.0;
        blue = 1.;
       // rainbow = vec3(1.,1.,1.);
    }
    color =vec4(red,green,blue,1.);

}
