#version 330 core
layout (location = 0) in vec3 vertex;
layout (location = 1) in vec2 uv;

out vec2 uvOut;
uniform mat3 scale;
uniform mat3 tran;
void main() {
    gl_Position = vec4(scale*tran*vertex,1.);
    uvOut = uv;
}
