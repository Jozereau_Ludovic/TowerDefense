#version 330 core

out vec4 color;
uniform float alpha;
uniform int end;


void main() {
    float al = alpha;
    if(end == 0){
        color = vec4(0.8,0.3,0,1.);
    }else{
        color = vec4(1-al);
     /*   if(al > 0.8){
            al = 0.8;
        }*/
        color.a = al;
    }

}
