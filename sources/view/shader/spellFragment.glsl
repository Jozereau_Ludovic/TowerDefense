#version 330 core

out vec4 colors;

in vec2 uvOut;
in vec2 uvText;
uniform float cooldown;
uniform float currentCooldown;
uniform sampler2D texture2D;
uniform int wait;
uniform float surbrillance;
const float pi = 3.14;

void main() {
      vec4 color;
     if (wait == 1){
            color = vec4(0.0, 0., 0., 1.);
     }else{
         color = texture(texture2D,uvText).rgba;
     }

     // on calcule l'angle sur lequel on va se basé, en radians
     float angle = 360*currentCooldown/cooldown;
     angle = radians(angle) ;

    float angle2 = atan(uvOut.y, uvOut.x) - pi/2;
     if (angle2 < 0){
              angle2 += 2*pi;
     }
     vec4 realColor = color;

     vec4 black = realColor - vec4(0.5,0.5,0.5,0);
     if(wait == 1){
        black = realColor;
        realColor =  vec4(1., 0., 0., 1.);
     }

     if (realColor.a == 0){
        black = vec4(0,0,0,0.5);
        if(wait == 1){
            black = vec4(1., 0., 0., 1.);
        }
     }

     if(angle < angle2){

        color = realColor;
     }else{

        color = black;
     }
     if(angle >= 2*pi){
          color = realColor;

     }
     if(wait == 1){
            if(length(uvOut) > 1 ){
                color = vec4(color.rgb, 0.);
            }else{
                if(length(uvOut)< 0.8){
                    color = vec4(1., 1., 1., 0.8);
                }
            }
     }


    colors.rgb = color.rgb + surbrillance;
    colors.a = color.a;

}
