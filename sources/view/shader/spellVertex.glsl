#version 330 core

layout ( location = 0) in vec3 vertices;
layout (location = 1) in vec2 uvTextIn;
layout ( location = 2) in vec2 uv;

uniform mat3 scale;
uniform mat3 tran;
uniform vec2 hg;
uniform vec2 bd;
out vec2 uvOut;
out vec2 uvText;
void main() {
    gl_Position = vec4(scale*tran*vertices,1.);
    uvOut = uv;
    uvText = mix(hg,hg+bd,uvTextIn);
   // uvText = uvTextIn;

}
