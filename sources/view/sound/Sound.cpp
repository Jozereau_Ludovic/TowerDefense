//
// Created by unnom on 20/03/16.
//

#include <exception>
#include <stdexcept>
#include <view/sound/Sound.hpp>
#include <iostream>

bool Sound::pause = false;
int Sound::volume = MIX_MAX_VOLUME/2;

Sound::Sound(){
    if(Mix_OpenAudio(44100, AUDIO_S16SYS, 2, 1024)){
        throw std::runtime_error(Mix_GetError());
    }


    sons["mort"] = Mix_LoadWAV("../resources/sound/0477.wav");
   // sons["mort"] = Mix_LoadWAV("../resources/sound/lapin.wav");
    sons["touche"] = Mix_LoadWAV("../resources/sound/0739.wav");


    for(auto i : sons) {
        if(!i.second){
            throw std::runtime_error(Mix_GetError());
        }
    }


}


Sound::~Sound(){
    Mix_HaltChannel(-1);
    for(auto i : sons) {
        Mix_FreeChunk(i.second);
    }
    Mix_CloseAudio();

}
void Sound::play(std::string son) {
    int i;
    for (i = 0; i<chanelUsed;i++) { //just right
        if (Mix_Playing(i))
            continue;
        break;
    }
    if(i==chanelUsed){
        Mix_AllocateChannels(i+2);
        chanelUsed++;
    }


    Mix_Volume(i, volume);
    Mix_PlayChannel(i,sons[son], 0);


}


void Sound::pauseAll() {
    pause = !pause;
    if(pause) {
        volume = 0;
    }else{
        volume = MIX_MAX_VOLUME/2;
    }

}

bool Sound::getPause() {
    return pause;
}

