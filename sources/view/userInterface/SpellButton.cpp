//
// Created by stiven on 07/04/16.
//

#include <functional>
#include "view/userInterface/SpellButton.hpp"



SpellButton::SpellButton(int x, int y, int width, int height, utils::element type, Action action, float cooldown,
                         std::string key)
:Button(x, y, width, height, type),cooldown(cooldown),currentCooldown(cooldown),action(action),key(key), range(0){

}

SpellButton::SpellButton(int x, int y, float range, utils::element type, SpellButton::Action action, std::string key): Button(x, y, (int)range, (int)range, type), action(action), key(key), range(range){

}

void SpellButton::useButton() {
    if(!active){
        if(action())
            enable();
    }

}


void SpellButton::enable() {
    active = !active;
}

void SpellButton::decreaseCooldown() {
    currentCooldown --;
    if(!currentCooldown){
        enable();
        currentCooldown = cooldown;
    }

}



bool SpellButton::isActive() {
    return active;
}


float SpellButton::getCooldown() {
    return cooldown;
}

float SpellButton::getCurrent() {
    return currentCooldown;
}


std::string SpellButton::getKey() {
    return key;
}

float SpellButton::getRange() {
    return range;
}






